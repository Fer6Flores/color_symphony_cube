#include <Arduino.h>

//https://github.com/pschatzmann/ESP32-A2DP
#include "BluetoothA2DPSink.h" 
// #include "BluetoothA2DPSource.h" // Transmit

BluetoothA2DPSink a2dp_sink; // Receive
// BluetoothA2DPSource a2dp_source; // Transmit

/*
void data_received_callback() {
  Serial.println("Data packet received");
}

void read_data_stream(const uint8_t *data, uint32_t length)
{

}
*/

static const i2s_config_t i2s_config = {
  .mode = (i2s_mode_t) (I2S_MODE_MASTER | I2S_MODE_TX | I2S_MODE_DAC_BUILT_IN),
  .sample_rate = 44100, // corrected by info from bluetooth
  .bits_per_sample = (i2s_bits_per_sample_t) 16, /* the DAC module will only take the 8bits from MSB */
  .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
  .communication_format = I2S_COMM_FORMAT_I2S_MSB,
  .intr_alloc_flags = 0, // default interrupt priority
  .dma_buf_count = 8,
  .dma_buf_len = 64,
  .use_apll = false
};

void setup() {

  a2dp_sink.set_i2s_config(i2s_config);
  a2dp_sink.start("ESP32_music");

/*
  // This function is needed to be notified when a packat is received.
  a2dp_sink.set_on_data_received(data_received_callback);

  // This function is needed to access the packet sended from the mobile
  a2dp_sink.set_stream_reader(read_data_stream);
  */
}

void loop() {

}