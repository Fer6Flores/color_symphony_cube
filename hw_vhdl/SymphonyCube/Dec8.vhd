-- Dec8
-- 3 bit decoder

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Dec8 is
    port ( 
		en      : in  STD_LOGIC;
		dec_in  : in  STD_LOGIC_VECTOR (2 downto 0);
      dec_out : out STD_LOGIC_VECTOR (7 downto 0)
	 );
end Dec8;

architecture Behavioral of Dec8 is
signal out_tmp : std_logic_vector (7 downto 0);
begin
	with dec_in select out_tmp <=
		"00000001" when "000",
		"00000010" when "001",
		"00000100" when "010",
		"00001000" when "011",
		"00010000" when "100",
		"00100000" when "101",
		"01000000" when "110",
		"10000000" when "111",
		(out_tmp'range => '0') when others;
		
		dec_out <= out_tmp when en = '1' else (dec_out'range => '0');
end Behavioral;

