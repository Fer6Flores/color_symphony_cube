-- Dec6
-- 3 bit decoder

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Dec6 is
    port ( 
		en      : in  STD_LOGIC;
		dec_in  : in  STD_LOGIC_VECTOR (2 downto 0);
      dec_out : out STD_LOGIC_VECTOR (5 downto 0)
	 );
end Dec6;

architecture Behavioral of Dec6 is
signal out_tmp : std_logic_vector (5 downto 0);
begin
	with dec_in select out_tmp <=
		"000001" when "000",
		"000010" when "001",
		"000100" when "010",
		"001000" when "011",
		"010000" when "100",
		"100000" when "101",
		(out_tmp'range => '0') when others;
		
		dec_out <= out_tmp when en = '1' else (dec_out'range => '0');
end Behavioral;

