-- RGB 6x6x6 Cube Framebuffer memory "VRAM"
-- Version 1. Dual ported memory

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


use IEEE.NUMERIC_STD.ALL;



entity Framebuffer is
	generic (
		N : integer := 1024
	);

    port ( 
		clk     : in  STD_LOGIC;
      addr_a  : in  STD_LOGIC_VECTOR (9 downto 0);
      addr_b  : in  STD_LOGIC_VECTOR (9 downto 0);
      write_a : in  STD_LOGIC_VECTOR (7 downto 0);
      we_a    : in  STD_LOGIC;
		data_a  : out STD_LOGIC_VECTOR (7 downto 0);
      data_b  : out STD_LOGIC_VECTOR (7 downto 0)
	);
end Framebuffer;

architecture Behavioral of Framebuffer is

type ram_cell is array (0 to (N-1)) of std_logic_vector(7 downto 0);
signal ram : ram_cell;

begin
	ram_write : process(clk)
	begin
		if rising_edge(clk) then
			if we_a = '1' and (unsigned(addr_a) < N) then
				ram(to_integer(unsigned(addr_a))) <= write_a;
			end if;
		end if;
	end process ram_write;
	
	
	
	data_a <= ram(to_integer(unsigned(addr_a))) when unsigned(addr_a) < N else (data_a'range => '0');
	data_b <= ram(to_integer(unsigned(addr_b))) when unsigned(addr_b) < N else (data_b'range => '0');
end Behavioral;

