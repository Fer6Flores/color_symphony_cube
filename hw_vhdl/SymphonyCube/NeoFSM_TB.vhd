--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   18:03:31 02/04/2021
-- Design Name:   
-- Module Name:   /home/brais/Escritorio/color_symphony_cube/hw_vhdl/SymphonyCube/NeoFSM_TB.vhd
-- Project Name:  SymphonyCube
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: NeoFSM
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY NeoFSM_TB IS
END NeoFSM_TB;
 
ARCHITECTURE behavior OF NeoFSM_TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT NeoFSM
    PORT(
         clk : IN  std_logic;
         ce : IN  std_logic;
         reset : IN  std_logic;
         next_word : IN  std_logic;
         reg_load : OUT  std_logic_vector(2 downto 0);
         reg_load_en : OUT  std_logic;
         fsm_idle : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal ce : std_logic := '0';
   signal reset : std_logic := '0';
   signal next_word : std_logic := '0';

 	--Outputs
   signal reg_load : std_logic_vector(2 downto 0);
   signal reg_load_en : std_logic;
   signal fsm_idle : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: NeoFSM PORT MAP (
          clk => clk,
          ce => ce,
          reset => reset,
          next_word => next_word,
          reg_load => reg_load,
          reg_load_en => reg_load_en,
          fsm_idle => fsm_idle
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      reset <= '1';
		wait for 100 ns;	
		reset <= '0';
      wait for clk_period*10;
		ce <= '1';
		next_word <= '0';
		-- FSM in IDLE
		wait for clk_period * 8;
		next_word <= '1';
		wait for clk_period * 2;
		next_word <= '0';
		wait for clk_period * 20;
		
		
		

		assert(false) report "Fin" severity FAILURE;
      wait;
   end process;

END;
