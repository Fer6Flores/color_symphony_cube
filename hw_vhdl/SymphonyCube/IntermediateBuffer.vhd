-- RGB 6x6x6 Intermediate Framebuffer memory for NeoVE (Neo Voxel Engine)
-- Version 1. Dual ported memory


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;



entity IntermediateBuffer is
	generic (
		N : integer := 8
	);

    port ( 
		clk     : in  STD_LOGIC;
      addr_a  : in  STD_LOGIC_VECTOR (2 downto 0);
      addr_b  : in  STD_LOGIC_VECTOR (2 downto 0);
      write_a : in  STD_LOGIC_VECTOR (23 downto 0);
      we_a    : in  STD_LOGIC;
		data_a  : out STD_LOGIC_VECTOR (23 downto 0);
      data_b  : out STD_LOGIC_VECTOR (23 downto 0)
	);
end IntermediateBuffer;

architecture Behavioral of IntermediateBuffer is

type ram_cell is array (0 to (N-1)) of std_logic_vector(23 downto 0);
signal ram : ram_cell;

begin
	ram_write : process(clk)
	begin
		if rising_edge(clk) then
			if we_a = '1' and (unsigned(addr_a) < N) then
				ram(to_integer(unsigned(addr_a))) <= write_a;
			end if;
		end if;
	end process ram_write;
	
	
	
	data_a <= ram(to_integer(unsigned(addr_a))) when unsigned(addr_a) < N else (data_a'range => '0');
	data_b <= ram(to_integer(unsigned(addr_b))) when unsigned(addr_b) < N else (data_b'range => '0');
end Behavioral;

