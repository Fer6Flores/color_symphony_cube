-- ClkPrescaler (Divisor de reloj configurable)
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ClkPrescaler is
    port (
		clk        : in  STD_LOGIC;
       ce        : in  STD_LOGIC;
		 reset     : in  STD_LOGIC;
		 reg_data  : in  STD_LOGIC_VECTOR (1 downto 0);
		reg_write  : in  STD_LOGIC;
		clk_out    : out  STD_LOGIC
	 );
end ClkPrescaler;

architecture Behavioral of ClkPrescaler is
	component Reg
		generic (
			N : integer := 8
		);

		 port ( 
			clk     : in  STD_LOGIC;
			reset   : in  STD_LOGIC;
			en      : in  STD_LOGIC;
			load    : in  STD_LOGIC_VECTOR ((N-1) downto 0);
			data    : out  STD_LOGIC_VECTOR ((N-1) downto 0)
		 );
	end component;
	
	component ClkTcGen
		generic (
	   -- Genera pulso a 1 kHz con un reloj de 50 MHz
		DIV : integer := 49999
	 );
	 
    port (
	   clk    : in  STD_LOGIC;
      ce     : in  STD_LOGIC;
      reset  : in  STD_LOGIC;
      tc     : out  STD_LOGIC := '0'
	 );
	end component;
	
	
	signal prescalerDiv1     : std_logic;
	signal prescalerDiv32    : std_logic;
	signal prescalerDiv512   : std_logic; 
	signal prescalerDiv4096  : std_logic;
	
	signal mux_reg           : std_logic_vector (1 downto 0);
begin
	-- Divisor de reloj 1
	prescalerDiv1 <= ce;
	
	-- Divisor de reloj 32
	Div32 : ClkTcGen
	GENERIC MAP (
		DIV => integer(31)
	)
	PORT MAP (
		clk   => clk,
		ce    => ce,
		reset => reset,
		tc    => prescalerDiv32
	);
	
	-- Divisor de reloj 512
	Div512 : ClkTcGen
	GENERIC MAP (
		DIV => integer(511)
	)
	PORT MAP (
		clk   => clk,
		ce    => ce,
		reset => reset,
		tc    => prescalerDiv512
	);
	
	-- Divisor de reloj 4096
	Div4096 : ClkTcGen
	GENERIC MAP (
		DIV => integer(4095)
	)
	PORT MAP (
		clk   => clk,
		ce    => ce,
		reset => reset,
		tc    => prescalerDiv4096
	);
	
	
	-- Registro de selección
	SelectReg : Reg
	GENERIC MAP (
		N => integer(2)
	)
	PORT MAP (
		clk   => clk,
		reset => reset,
		en    => reg_write,
		load  => reg_data,
		data  => mux_reg
	);
	
	-- Multiplexor de salida
	with mux_reg select clk_out <=
		prescalerDiv1    when "00",
		prescalerDiv32   when "01",
		prescalerDiv512  when "10",
		prescalerDiv4096 when "11",
		'0'              when others;


end Behavioral;

