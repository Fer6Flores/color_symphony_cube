-- General purpose N bit register
-- Designed for SymphonyCube


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;


entity Reg is
	 generic (
		 N : integer := 8
	 );

    port ( 
		clk     : in  STD_LOGIC;
		reset   : in  STD_LOGIC;
      en      : in  STD_LOGIC;
      load    : in  STD_LOGIC_VECTOR ((N-1) downto 0);
      data    : out STD_LOGIC_VECTOR ((N-1) downto 0)
	 );
end Reg;

architecture Behavioral of Reg is
signal internal : std_logic_vector ((N-1) downto 0) := (others => '0');
begin
	data <= internal;

	process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				internal <= (others => '0');
			elsif en = '1' then
				internal <= load;
			else
				internal <= internal;
			end if;	
		end if;
		
	end process;
end Behavioral;

