-- ClkTcGen: Contador genérico con salida tc
-- Utilizado como divisor de reloj
-- Diseño por Brais Solla González. Propósito general
-- Reutilizado del proyecto TetrisN2

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ClkTcGen is
	 generic (
	   -- Genera pulso a 1 kHz con un reloj de 50 MHz
		DIV : integer := 49999
	 );
	 
    port (
	   clk    : in  STD_LOGIC;
      ce     : in  STD_LOGIC;
      reset  : in  STD_LOGIC;
      tc     : out  STD_LOGIC := '0'
	 );
end ClkTcGen;

architecture Behavioral of ClkTcGen is
signal count : integer range 0 to DIV := 0;
begin
	process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				count <= 0;
				tc <= '0';
			elsif ce = '1' then
				if count = DIV then
					tc <= '1';
					count <= 0;
				else
					tc <= '0';
					count <= count+1;
				end if;
			else
				count <= count;
			end if;
		end if;
	end process;
end Behavioral;

