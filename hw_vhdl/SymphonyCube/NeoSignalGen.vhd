-- NeoSignalGen. Logic 0 and 1 signal generator for NeoPixel-like LED strips
-- Designed for project SymphonyCube. Version 1


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

entity NeoSignalGen is
	generic (
		COUNT_MAX : integer := 250;
		-- According to the datasheet. Asumming 50 MHz clock frequency.
		-- T0
		T0H       : integer := 50;
		T0L       : integer := 200;
		-- T1
		T1H       : integer := 120;
		T1L       : integer := 130
	);

	port ( 
		clk   : in  STD_LOGIC;
      reset : in  STD_LOGIC;
      ce    : in  STD_LOGIC;
      
		out_0 : out  STD_LOGIC;
      out_1 : out  STD_LOGIC;
      next_bit : out  STD_LOGIC
	  );
end NeoSignalGen;

architecture Behavioral of NeoSignalGen is
signal internal : integer range 0 to COUNT_MAX := 0;
begin
	cnt : process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				internal <= 0;
			elsif ce = '1' then
				if internal = COUNT_MAX then
					internal <= 0;
				else
					internal <= internal + 1;
				end if;
			end if;
		end if;
	end process cnt;
	
	-- OUT0, OUT1 and NEXT_BIT
	out_0 <= '1' when internal <= T0H else '0';
	out_1 <= '1' when internal <= T1H else '0';
	
	next_bit <= '1' when internal = COUNT_MAX else '0';
	
end Behavioral;

