-- shift register (Parallel In, Serial Out)
-- Designed for Symphony Cube "Neo" NeoPixel driver

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


use IEEE.NUMERIC_STD.ALL;


entity NeoShftRegPISO is
	generic (
		N : integer := 8
	);

    port ( 
		clk   : in  STD_LOGIC;
      reset : in  STD_LOGIC;
      ce    : in  STD_LOGIC;
      mode  : in  STD_LOGIC;
      load  : in  STD_LOGIC_VECTOR ((N-1) downto 0);
      s_out : out  STD_LOGIC
	);
end NeoShftRegPISO;

architecture Behavioral of NeoShftRegPISO is
signal internal : std_logic_vector ((N-1) downto 0) := (others => '0');
begin
	s_out <= internal(N-1);
	process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				internal <= (others => '0');
			elsif ce = '1' then
				if mode = '1' then
					internal <= load;
				elsif mode = '0' then
					internal <= internal ((N-2) downto 0) & "0";
				else
					internal <= internal;
				end if;
			end if;
		end if;
	end process;
end Behavioral;

