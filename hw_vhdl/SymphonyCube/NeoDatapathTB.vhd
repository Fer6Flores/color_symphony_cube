--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:58:55 11/03/2020
-- Design Name:   
-- Module Name:   /media/brais/HDD/Xilinx/color_symphony_cube/hw_vhdl/SymphonyCube/NeoDatapathTB.vhd
-- Project Name:  SymphonyCube
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: NeoDatapath
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY NeoDatapathTB IS
END NeoDatapathTB;
 
ARCHITECTURE behavior OF NeoDatapathTB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT NeoDatapath
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         ce : IN  std_logic;
         ge : IN  std_logic;
         data_bus : IN  std_logic_vector(23 downto 0);
         en_load : IN  std_logic_vector(5 downto 0);
         leds_reset : IN  std_logic;
         leds_ctrl : OUT  std_logic_vector(5 downto 0);
         next_word : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal ce : std_logic := '0';
   signal ge : std_logic := '0';
   signal data_bus : std_logic_vector(23 downto 0) := (others => '0');
   signal en_load : std_logic_vector(5 downto 0) := (others => '0');
   signal leds_reset : std_logic := '0';

 	--Outputs
   signal leds_ctrl : std_logic_vector(5 downto 0);
   signal next_word : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: NeoDatapath PORT MAP (
          clk => clk,
          reset => reset,
          ce => ce,
          ge => ge,
          data_bus => data_bus,
          en_load => en_load,
          leds_reset => leds_reset,
          leds_ctrl => leds_ctrl,
          next_word => next_word
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      reset <= '1';
      wait for 100 ns;	
		reset <= '0';
      wait for clk_period*10;

		leds_reset <= '0';
      -- Clock enable = 1
		ce <= '1';
		ge <= '0';
		-- Generator enable = 0, STATE = load words
		-- Load first PISO Shift Register
		data_bus <= "000000000000000000000000";
		en_load  <= "000001";
		wait for clk_period;
		-- Second...
		data_bus <= "111111111111111111111111";
		en_load  <= "000010";
		wait for clk_period;
		-- 3...
		data_bus <= "001100110011001100110011";
		en_load  <= "000100";
		wait for clk_period;
		-- 4...
		data_bus <= "110011001100110011001100";
		en_load  <= "001000";
		wait for clk_period;
		-- 5...
		data_bus <= "000011110000111100001111";
		en_load  <= "010000";
		wait for clk_period;
		-- 6...
		data_bus <= "111100001111000011110000";
		en_load  <= "100000";
		wait for clk_period;
		
		data_bus <= (others => '0');
		en_load  <= (others => '0');
		wait for 4 * clk_period; 
		
		-- Ready...
		ge <= '1';
		-- Go
		
		wait for 1000 * 8 * clk_period;
		
		-- Test led RESET
		leds_reset <= '1';
		wait for 1000 * 8 * clk_period;
		
		leds_reset <= '0';
		-- All zeroes
		
		wait for 1000 * 8 * clk_period;


		assert(false) report "Fin" severity FAILURE;
      wait;
   end process;

END;
