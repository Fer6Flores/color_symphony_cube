-- Mux2

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Mux2 is
    Port ( 
		in_a : in  STD_LOGIC;
      in_b : in  STD_LOGIC;
      sel  : in  STD_LOGIC;
		
      mux_out : out STD_LOGIC
	  );
end Mux2;

architecture Behavioral of Mux2 is
begin

	mux_out <= in_a when sel = '0' else in_b;
end Behavioral;

