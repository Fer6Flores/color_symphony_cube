--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:38:51 08/11/2020
-- Design Name:   
-- Module Name:   /media/brais/HDD/Xilinx/14.7/ISE_DS/SymphonyCube/ClkPrescalerTB.vhd
-- Project Name:  SymphonyCube
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ClkPrescaler
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY ClkPrescalerTB IS
END ClkPrescalerTB;
 
ARCHITECTURE behavior OF ClkPrescalerTB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ClkPrescaler
    PORT(
         clk : IN  std_logic;
         ce : IN  std_logic;
         reset : IN  std_logic;
         reg_data : IN  std_logic_vector(1 downto 0);
         reg_write : IN  std_logic;
         clk_out : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal ce : std_logic := '0';
   signal reset : std_logic := '0';
   signal reg_data : std_logic_vector(1 downto 0) := (others => '0');
   signal reg_write : std_logic := '0';

 	--Outputs
   signal clk_out : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns; 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ClkPrescaler PORT MAP (
          clk => clk,
          ce => ce,
          reset => reset,
          reg_data => reg_data,
          reg_write => reg_write,
          clk_out => clk_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      reset <= '1';
      wait for 100 ns;
		reset <= '0';
      wait for clk_period*10;
		
		-- Set prescaler = 1
		ce <= '1';
		reg_data <= "00";
		wait for clk_period;
		reg_write <= '1';
		wait for clk_period;
		reg_write <= '0';
		
		wait for 8192 * clk_period;
		
		-- Set prescaler = 32
		ce <= '1';
		reg_data <= "01";
		wait for clk_period;
		reg_write <= '1';
		wait for clk_period;
		reg_write <= '0';
		
		wait for 8192 * clk_period;
		
		-- Set prescaler = 512
		ce <= '1';
		reg_data <= "10";
		wait for clk_period;
		reg_write <= '1';
		wait for clk_period;
		reg_write <= '0';
		
		wait for 8192 * clk_period;
		
		-- Set prescaler = 4096
		ce <= '1';
		reg_data <= "11";
		wait for clk_period;
		reg_write <= '1';
		wait for clk_period;
		reg_write <= '0';
		
		reg_data <= "00";
		
		wait for 8192 * clk_period;
      

      assert(false) report "Fin" severity FAILURE;
		wait;
   end process;

END;
