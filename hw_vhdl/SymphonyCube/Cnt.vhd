-- Cnt: Contador de N bits de propósito general
-- Por defecto se instancia un contador de 8 bits, máximo valor 255
-- Diseño por Brais Solla González. Propósito general
-- Reutilizado del proyecto TetrisN2

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity Cnt is
	generic (
		N   : integer := 8;
		MAX : integer := 255
	
	);
	
   port ( 
		clk : in  STD_LOGIC;
      reset : in  STD_LOGIC;
      ce : in  STD_LOGIC;
      count : out  STD_LOGIC_VECTOR ((N-1) downto 0);
      tc : out  STD_LOGIC
	);
	
end Cnt;

architecture Behavioral of Cnt is
signal internal : std_logic_vector ((N-1) downto 0) := (others => '0');
begin
	count <= internal;
	tc    <= '1' when (internal = MAX) else '0';
	
	process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				internal <= (others => '0');
			elsif ce = '1' then
				if internal = MAX then
					internal <= (others => '0');
				else
					internal <= internal + 1;
				end if;
			else
				internal <= internal;
			end if;
		end if;
	end process;
end Behavioral;

