-- NeoFSM version 1
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


use IEEE.NUMERIC_STD.ALL;

entity NeoFSM is
    Port (
		clk   : in  STD_LOGIC;
      ce    : in  STD_LOGIC;
      reset : in  STD_LOGIC;
		
		-- Next word load request
      next_word : in STD_LOGIC;
		
		-- Outputs FSM => Datapath
		reg_load    : out STD_LOGIC_VECTOR (2 downto 0);
		reg_load_en : out STD_LOGIC;
		
		-- Outputs from the FSM
		fsm_idle    : out STD_LOGIC
	);
end NeoFSM;

architecture Behavioral of NeoFSM is
	type FSM_State is (FSM_RESET, IDLE, LOAD_0, LOAD_0L, LOAD_1, LOAD_1L, LOAD_2, LOAD_2L, LOAD_3, LOAD_3L, LOAD_4, LOAD_4L, LOAD_5, LOAD_5L);
	signal state : FSM_State := FSM_RESET;
	
begin
		fsm_nextstate : process(clk)
		begin
			if rising_edge(clk) then
				if reset = '1' then
					state <= FSM_RESET;
				elsif ce = '1' then
					if state = FSM_RESET then
						state <= IDLE;
					elsif state = IDLE then
						if next_word = '1' then
							state <= LOAD_0;
						else
							state <= IDLE;
						end if;
					elsif state = LOAD_0 then
						state <= LOAD_0L;
					elsif state = LOAD_0L then
						state <= LOAD_1;
					elsif state = LOAD_1 then
						state <= LOAD_1L;
					elsif state = LOAD_1L then
						state <= LOAD_2;
					elsif state = LOAD_2 then
						state <= LOAD_2L;
					elsif state = LOAD_2L then
						state <= LOAD_3;
					elsif state = LOAD_3 then
						state <= LOAD_3L;
					elsif state = LOAD_3L then
						state <= LOAD_4;
					elsif state = LOAD_4 then
						state <= LOAD_4L;
					elsif state = LOAD_4L then
						state <= LOAD_5;
					elsif state = LOAD_5 then
						state <= LOAD_5L;
					elsif state = LOAD_5L then
						state <= IDLE;
					else
						state <= state;
					end if;			
				end if;
			end if;
		end process fsm_nextstate;
		
		fsm_outputs : process(clk, state)
		begin
			if state = FSM_RESET then
				reg_load    <= "000";
				reg_load_en <= '0';
				fsm_idle    <= '0';
			elsif state = IDLE then
				reg_load    <= "000";
				reg_load_en <= '0';
				fsm_idle    <= '1';
			elsif state = LOAD_0 then
				reg_load    <= "000";
				reg_load_en <= '0';
				fsm_idle    <= '0';
			elsif state = LOAD_0L then
				reg_load    <= "000";
				reg_load_en <= '1';
				fsm_idle    <= '0';
			elsif state = LOAD_1 then
				reg_load    <= "001";
				reg_load_en <= '0';
				fsm_idle    <= '0';
			elsif state = LOAD_1L then
				reg_load    <= "001";
				reg_load_en <= '1';
				fsm_idle    <= '0';
			elsif state = LOAD_2 then
				reg_load    <= "010";
				reg_load_en <= '0';
				fsm_idle    <= '0';
			elsif state = LOAD_2L then
				reg_load    <= "010";
				reg_load_en <= '1';
				fsm_idle    <= '0';
			elsif state = LOAD_3 then
				reg_load    <= "011";
				reg_load_en <= '0';
				fsm_idle    <= '0';
			elsif state = LOAD_3L then
				reg_load    <= "011";
				reg_load_en <= '1';
				fsm_idle    <= '0';
			elsif state = LOAD_4 then
				reg_load    <= "100";
				reg_load_en <= '0';
				fsm_idle    <= '0';
			elsif state = LOAD_4L then
				reg_load    <= "100";
				reg_load_en <= '1';
				fsm_idle    <= '0';
			elsif state = LOAD_5 then
				reg_load    <= "101";
				reg_load_en <= '0';
				fsm_idle    <= '0';
			elsif state = LOAD_5L then
				reg_load    <= "101";
				reg_load_en <= '1';
				fsm_idle    <= '0';
			else
				reg_load    <= (others => '0');
				reg_load_en <= '0';
				fsm_idle    <= '0';
			end if;
		end process fsm_outputs;



end Behavioral;

