--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   19:23:36 07/20/2020
-- Design Name:   
-- Module Name:   /media/brais/HDD/Xilinx/14.7/ISE_DS/SymphonyCube/NeoShftRegPISO_TB.vhd
-- Project Name:  SymphonyCube
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: NeoShftRegPISO
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY NeoShftRegPISO_TB IS
END NeoShftRegPISO_TB;
 
ARCHITECTURE behavior OF NeoShftRegPISO_TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT NeoShftRegPISO
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         ce : IN  std_logic;
         mode : IN  std_logic;
         load : IN  std_logic_vector(7 downto 0);
         s_out : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal ce : std_logic := '0';
   signal mode : std_logic := '0';
   signal load : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal s_out : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: NeoShftRegPISO PORT MAP (
          clk => clk,
          reset => reset,
          ce => ce,
          mode => mode,
          load => load,
          s_out => s_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		reset <= '1';
      wait for 100 ns;	
		reset <= '0';
      wait for clk_period*10;

		load <= "10101111";
		wait for clk_period;
		mode <= '1';
		ce <= '1';
		wait for clk_period;
		ce <= '0';
		mode <= '0';
		
		wait for 4 * clk_period;
		ce <= '1';
		
		wait for 8 * clk_period;
		mode <= '1';
		wait for 8 * clk_period;
		mode <= '0';
		wait for 8 * clk_period;
		

      assert(false) report "Fin" severity FAILURE;
   end process;

END;
