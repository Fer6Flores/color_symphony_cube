--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:46:09 07/20/2020
-- Design Name:   
-- Module Name:   /media/brais/HDD/Xilinx/14.7/ISE_DS/SymphonyCube/NeoSignalGen_TB.vhd
-- Project Name:  SymphonyCube
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: NeoSignalGen
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY NeoSignalGen_TB IS
END NeoSignalGen_TB;
 
ARCHITECTURE behavior OF NeoSignalGen_TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT NeoSignalGen
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         ce : IN  std_logic;
         out_0 : OUT  std_logic;
         out_1 : OUT  std_logic;
         next_bit : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal ce : std_logic := '0';

 	--Outputs
   signal out_0 : std_logic;
   signal out_1 : std_logic;
   signal next_bit : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: NeoSignalGen PORT MAP (
          clk => clk,
          reset => reset,
          ce => ce,
          out_0 => out_0,
          out_1 => out_1,
          next_bit => next_bit
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
		reset <= '1';
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		reset <= '0';

      wait for clk_period*10;
		ce <= '1';
		
		wait for 125 * 10 * clk_period; -- 10 cycles

      assert(false) report "Fin" severity FAILURE;
   end process;

END;
