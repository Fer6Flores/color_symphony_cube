--------------------------------------------------------------
--------------------- Generic register -----------------------
--------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity register_n is
	 Generic ( N 		: integer range 0 to 255 := 8);
    Port ( RESET 		: in STD_LOGIC;
           CLK 		: in STD_LOGIC;
           EN 			: in STD_LOGIC;
           DATA_IN 	: in STD_LOGIC_VECTOR (N-1 downto 0);
           DATA_OUT 	: out STD_LOGIC_VECTOR (N-1 downto 0));
end register_n;

architecture Behavioral of register_n is

-- Signals
signal data_out_i : STD_LOGIC_VECTOR (N-1 downto 0) := (others => '0');

begin

-- Assignments
DATA_OUT <= data_out_i;

-- Process
process (CLK)
	begin
		if CLK'event and CLK = '1' then
			if RESET = '1' then
				data_out_i <= (others => '0');
			elsif EN = '1' then
				data_out_i <= DATA_IN;
			end if;
		end if;
end process;

end Behavioral;

