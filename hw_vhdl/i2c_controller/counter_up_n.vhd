--------------------------------------------------------------
-------------------- Generic counter up ----------------------
--------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity counter_up_n is
	 Generic ( N 	: integer range 0 to 31 := 5);
    Port ( RESET 	: in STD_LOGIC;
           CLK 	: in STD_LOGIC;
           EN 		: in STD_LOGIC;
           COUNT 	: out STD_LOGIC_VECTOR (N-1 downto 0)
			  );
end counter_up_n;

architecture Behavioral of counter_up_n is

-- Signals
signal count_i : UNSIGNED (N-1 downto 0) := (others => '0');

begin

-- Assignments
COUNT <= std_logic_vector(count_i);

-- Process
process (CLK) 
	begin
		if CLK'event and CLK = '1' then
			if RESET='1' then 
				count_i <= (others => '0');
			elsif EN='1' then
				count_i <= count_i + 1;
			end if;
		end if;
end process; 

end Behavioral;