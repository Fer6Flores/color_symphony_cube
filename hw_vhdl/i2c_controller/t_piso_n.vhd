LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY t_piso_n IS
END t_piso_n;
 
ARCHITECTURE behavior OF t_piso_n IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT piso_n
    PORT(
         RESET : IN  std_logic;
         CLK : IN  std_logic;
         EN : IN  std_logic;
         LOAD_EN : IN  std_logic;
         DATA_IN : IN  std_logic_vector(23 downto 0);
         DATA_OUT : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal RESET : std_logic := '0';
   signal CLK : std_logic := '0';
   signal EN : std_logic := '0';
   signal LOAD_EN : std_logic := '0';
   signal DATA_IN : std_logic_vector(23 downto 0) := (others => '0');

 	--Outputs
   signal DATA_OUT : std_logic;

   -- Clock period definitions
   constant CLK_period : time := 2500 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: piso_n PORT MAP (
          RESET => RESET,
          CLK => CLK,
          EN => EN,
          LOAD_EN => LOAD_EN,
          DATA_IN => DATA_IN,
          DATA_OUT => DATA_OUT
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		wait for 100 ns;
		
		RESET <= '1';
      wait for CLK_period*5+CLK_period/2;
		
		RESET <= '0';	
		DATA_IN <= "111100001100110011101011";
		
      wait for CLK_period;
		LOAD_EN <= '1';		
		wait for CLK_period*2;

		EN <= '1';		
		wait for CLK_period;

		LOAD_EN <= '0';
		EN <= '0';		
		wait for CLK_period;
		
		DATA_IN <= (others => '0');
		EN <= '1';		
		wait for CLK_period*8;

		EN <= '0';
		wait for CLK_period;

		EN <= '1';
		wait for CLK_period*8;

		EN <= '0';
		wait for CLK_period;

		EN <= '1';
		wait for CLK_period*8;
		
		EN <= '0';
      wait;
   end process;

END;
