--------------------------------------------------------------
------------------------ Data path ---------------------------
--------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity data_path is
	 Generic ( MAX_NUM_CNTS_SAMPLES : integer range 0 to 31 := 7);
																					
    Port ( 
				-- GENERAL
				CLK_IN 					: in STD_LOGIC;
				CLK_I2C_AUX_3200K 	: in STD_LOGIC;
				RESET 					: in STD_LOGIC;
				
				-- I2C BUS
				SDA						: inout  STD_LOGIC;
				SCL 						: inout  STD_LOGIC;
				
				-- INPUT SIGNALS TO SENSOR CONTROLLER
				SLAVE_ADDRESS 			: in  STD_LOGIC_VECTOR (6 downto 0);
				R_W 						: in  STD_LOGIC;
				REG_ADDRESS 			: in  STD_LOGIC_VECTOR (7 downto 0);
				DATA_TO_WRITE 			: in  STD_LOGIC_VECTOR (7 downto 0);
				CNTS_READ 				: in STD_LOGIC;
				
				-- OUTPUT SIGNALS TO SENSOR CONTROLLER
				HR_DATA 					: out  STD_LOGIC_VECTOR (7 downto 0);
				
				-- INPUT SIGNALS FROM CONTROL PATH
				NEW_START	 							: in STD_LOGIC;
				STOP_SCL_90DEG 						: in STD_LOGIC;
				SCL_MODE 								: in STD_LOGIC;
				SDA_MODE									: in STD_LOGIC;
				SDA_SELECT 								: in STD_LOGIC_VECTOR(1 downto 0);
				SDA_AM 									: in STD_LOGIC;
				SDA_ACK_REG_EN							: in STD_LOGIC;
				CNTS_SAMPLES_COUNTER_EN 			: in STD_LOGIC;
				COUNT8_EN_READING						: in STD_LOGIC;
				COUNT4_EN								: in STD_LOGIC;
				CNTS_SAMPLES_COUNTER_RESET_FSM 	: in STD_LOGIC;
				SIPO_EN_FSM 							: in STD_LOGIC;
				REG_OUT_EN 								: in STD_LOGIC;
				PISO_EN_FSM 							: in STD_LOGIC;
				SECOND_ADDR_WRITE_FLIP				: in STD_LOGIC;
				WRITING_COUNTER_RESET				: in STD_LOGIC;	
				TIMING_COUNTER_EN						: in STD_LOGIC;

				-- OUTPUT SIGNALS TO CONTROL PATH
				SCL_FD									: out STD_LOGIC;
				SCL_90DEG_FD							: out STD_LOGIC;
				SCL_90DEG_FA							: out STD_LOGIC;
				TC_TIMING_WAIT							: out STD_LOGIC;
				SDA_ACK_STORED							: out STD_LOGIC;
				CNTS_SAMPLES_TC 						: out STD_LOGIC;
				COUNT8_TC								: out STD_LOGIC;	
				COUNT4									: out STD_LOGIC_VECTOR(1 downto 0);
				R_W_STORED								: out STD_LOGIC;
				CNTS_READ_STORED						: out STD_LOGIC;
				SECOND_ADDR_WRITE_REG				: out STD_LOGIC
				);
				
end data_path;

architecture Behavioral of data_path is

-- Constants
constant data_in_length 	: INTEGER := 8;

-- SCL and 90 deg SCL signals and its correspondent edges
signal scl_i 					: STD_LOGIC := '1';
signal scl_fd_i 				: STD_LOGIC := '0';
signal scl_90deg_i 			: STD_LOGIC := '1';
signal scl_90deg_fa_i 		: STD_LOGIC := '0';
signal scl_90deg_fd_i 		: STD_LOGIC := '0';

-- Register enables
signal sipo_en_sync						: STD_LOGIC := '0';
signal piso_en_sync						: STD_LOGIC := '0';
signal sda_ack_reg_en_sync				: STD_LOGIC := '0';
signal second_addr_write_reg_i		: STD_LOGIC := '0';
signal second_addr_write_flip_sync	: STD_LOGIC := '0';

-- All data from sensor controller when a new order happens
signal rw_cnts_read_data 		: STD_LOGIC_VECTOR(1 downto 0) := (others => '0');
signal new_order_data			: STD_LOGIC_VECTOR(24 downto 0) := (others => '0');
signal new_order_data_sync		: STD_LOGIC_VECTOR(24 downto 0) := (others => '0');

-- Data to SDA serialized 
signal serial_slave_addres_rw : STD_LOGIC := '0'; 
signal serial_reg_address		: STD_LOGIC := '0'; 
signal serial_data_to_write	: STD_LOGIC := '0';

-- Data from SDA parallelized
signal sda_data_sipo 			: STD_LOGIC_VECTOR(7 downto 0) := (others => '0');

-- SDA high-Z selector signals
signal sda_data_in				: STD_LOGIC := '0';
signal sda_data_out				: STD_LOGIC := '0';

-- Counters enables and resets
signal samples_count						: STD_LOGIC_VECTOR(2 downto 0) := (others => '0'); --Uso de log2 
signal timing_count						: STD_LOGIC_VECTOR(5 downto 0) := (others => '0'); 
signal count8								: STD_LOGIC_VECTOR(2 downto 0) := (others => '0');
signal count8_en_sync					: STD_LOGIC := '0';
signal count4_en_sync					: STD_LOGIC := '0';
signal reset_cnts_samples_counter	: STD_LOGIC := '0';
signal cnts_samples_counter_en_sync	: STD_LOGIC := '0';
signal count4_counter_reset			: STD_LOGIC := '0';
signal reset_timing_counter			: STD_LOGIC := '0';

-- Frequency divider resets
signal reset_scl_gen						: STD_LOGIC := '0';
signal reset_scl_90deg					: STD_LOGIC := '0';

signal clk_i2c_aux_3200k_fa			: STD_LOGIC := '0';
signal clk_i2c_aux_3200k_fd			: STD_LOGIC := '0';



-------------------- Components --------------------
component edge_detector
	port(	clk		: in STD_LOGIC;
			reset		: in STD_LOGIC;
			pulse		: in STD_LOGIC;
			fa_pulse	: out STD_LOGIC;
			fd_pulse	: out STD_LOGIC
		);
end component;

COMPONENT freq_divider
	Generic(
		divider 		: INTEGER := 8;
		phase 		: INTEGER := 2);
	PORT(
		clock_in 	: in STD_LOGIC;
		reset 		: in STD_LOGIC;
		clock_out 	: out STD_LOGIC
		);
END COMPONENT;

COMPONENT piso_n
	Generic ( N 	: integer range 0 to 255 := 24);
	PORT(
		RESET 		: in STD_LOGIC;
		CLK 			: in STD_LOGIC;
		EN 			: in STD_LOGIC;
		LOAD_EN 		: in STD_LOGIC;
		DATA_IN 		: in STD_LOGIC_VECTOR(N-1 downto 0);          
		DATA_OUT 	: out STD_LOGIC
		);
	END COMPONENT;
	
COMPONENT sipo_n
	 Generic ( N 	: integer range 0 to 255 := 8);
	PORT(
		RESET 		: in STD_LOGIC;
		CLK 			: in STD_LOGIC;
		EN 			: in STD_LOGIC;
		DATA_IN 		: in STD_LOGIC;          
		DATA_OUT 	: out STD_LOGIC_VECTOR(N-1 downto 0)
		);
	END COMPONENT;
	
COMPONENT register_n
	 Generic ( N 	: integer range 0 to 255 := 8);
    Port ( 
		RESET 		: in  STD_LOGIC;
		CLK 			: in  STD_LOGIC;
      EN 			: in  STD_LOGIC;
      DATA_IN 		: in  STD_LOGIC_VECTOR (N-1 downto 0);
      DATA_OUT 	: out  STD_LOGIC_VECTOR (N-1 downto 0));
end COMPONENT;

COMPONENT counter_up_n
	 Generic ( N 	: integer range 0 to 31 := 5);
    Port ( RESET 	: in  STD_LOGIC;
           CLK 	: in  STD_LOGIC;
           EN 		: in  STD_LOGIC;
           COUNT 	: out STD_LOGIC_VECTOR (N-1 downto 0)
			  );
end COMPONENT;

begin

-------------------- Assignments --------------------
-- Data inputs concatenation
rw_cnts_read_data 				<= new_order_data_sync(17) & new_order_data_sync(0);
new_order_data 					<= SLAVE_ADDRESS & R_W & REG_ADDRESS & DATA_TO_WRITE & CNTS_READ;

-- Register enables synchronization
sipo_en_sync						<= (SIPO_EN_FSM and scl_90deg_fd_i);
piso_en_sync						<= (PISO_EN_FSM and scl_90deg_fa_i);
sda_ack_reg_en_sync				<= (SDA_ACK_REG_EN and scl_90deg_fd_i);
second_addr_write_flip_sync 	<= (SECOND_ADDR_WRITE_FLIP and scl_90deg_fd_i);

-- Counter enables synchronization
count4_en_sync						<= (COUNT4_EN and scl_90deg_fd_i);	
count8_en_sync						<= ((COUNT8_EN_READING and scl_90deg_fd_i) or piso_en_sync);
cnts_samples_counter_en_sync 	<= (CNTS_SAMPLES_COUNTER_EN and scl_90deg_fd_i);

-- Outputs assignments
SCL_FD								<= scl_fd_i;
SCL_90DEG_FD						<= scl_90deg_fd_i;
SCL_90DEG_FA						<= scl_90deg_fa_i;
SECOND_ADDR_WRITE_REG 			<= second_addr_write_reg_i;

-- Frequency divider resets
reset_scl_gen 						<= (RESET or SCL_MODE);
reset_scl_90deg 					<= (RESET or STOP_SCL_90DEG);

-- Counter resets
reset_cnts_samples_counter 	<= (RESET or CNTS_SAMPLES_COUNTER_RESET_FSM);
count4_counter_reset 			<= (RESET or WRITING_COUNTER_RESET);
reset_timing_counter				<= (RESET or not TIMING_COUNTER_EN);

-------------------- Instances --------------------

-- A pair of little counters to generate SCL and SCL with a phase of 90�
scl_gen: freq_divider 
					GENERIC MAP(
						divider 		=> 8,
						phase 		=> 0
					)
					PORT MAP(
						clock_in 	=> CLK_I2C_AUX_3200K,
						reset 		=> reset_scl_gen,
						clock_out 	=> scl_i
					);

scl_90deg_gen: freq_divider 
					GENERIC MAP(
						divider 		=> 8,
						phase 		=> 2
					)
					PORT MAP(
						clock_in 	=> CLK_I2C_AUX_3200K,
						reset 		=> reset_scl_90deg,
						clock_out 	=> scl_90deg_i
					);

-- Edge detectors of SCL and SCL with a phase of 90�
scl_edge_detector: edge_detector 
					port map	(
						clk			=> CLK_IN,
						reset			=> RESET,
						pulse			=> scl_i, 
						fa_pulse		=> open,
						fd_pulse		=> scl_fd_i
					);

scl_90deg_edge_detector: edge_detector 
					port map	(
						clk			=> CLK_IN,
						reset			=> RESET,
						pulse			=> scl_90deg_i, 
						fa_pulse		=> scl_90deg_fa_i,
						fd_pulse		=> scl_90deg_fd_i
					);

-- Three PISO registers to store and serialize data from sensor controller
new_slave_address_rw_piso : piso_n 
					GENERIC MAP(
						N 				=> data_in_length
					)
					PORT MAP(
						RESET 		=> RESET,
						CLK 			=> CLK_IN,
						EN 			=> piso_en_sync,
						LOAD_EN 		=> NEW_START,
						DATA_IN 		=> new_order_data_sync(24 downto 17),
						DATA_OUT 	=> serial_slave_addres_rw
					);

new_reg_address_piso : piso_n 
					GENERIC MAP(
						N 				=> data_in_length
					)
					PORT MAP(
						RESET 		=> RESET,
						CLK 			=> CLK_IN,
						EN 			=> piso_en_sync,
						LOAD_EN 		=> NEW_START,
						DATA_IN 		=> new_order_data_sync(16 downto 9),
						DATA_OUT 	=> serial_reg_address
					);
					
new_data_to_write_piso : piso_n 
					GENERIC MAP(
						N 				=> data_in_length
					)
					PORT MAP(
						RESET 		=> RESET,
						CLK 			=> CLK_IN,
						EN 			=> piso_en_sync,
						LOAD_EN 		=> NEW_START,
						DATA_IN 		=> new_order_data_sync(8 downto 1),
						DATA_OUT 	=> serial_data_to_write
					);
					
-- New order signal register synchronization
new_order_data_delay_reg : register_n 
					GENERIC MAP(
						N 				=> 25
					)
					PORT MAP(
						RESET 		=> RESET,
						CLK 			=> CLK_IN,
						EN 			=> '1',
						DATA_IN 		=> new_order_data,
						DATA_OUT 	=> new_order_data_sync
					);

-- RW and CNTS_READ bits register
rw_cnts_read_delay_reg : register_n 
					GENERIC MAP(
						N 				=> 2
					)
					PORT MAP(
						RESET 		=> RESET,
						CLK 			=> CLK_IN,
						EN 			=> NEW_START,
						DATA_IN 		=> rw_cnts_read_data,
						DATA_OUT(1) => R_W_STORED,
						DATA_OUT(0) => CNTS_READ_STORED
					);

-- Mux to select the data bit to be introduced on the SDA line
sda_data_out_mux : process(serial_slave_addres_rw, serial_reg_address, serial_data_to_write, 
									SDA_AM, SDA_SELECT, SDA_MODE, count8, second_addr_write_reg_i)
	begin						
		case SDA_SELECT is
			when "00" => 
				if ((count8 = "111") and (second_addr_write_reg_i = '0')) then
					sda_data_out <= '0';
				elsif ((count8 = "111") and (second_addr_write_reg_i = '1')) then
					sda_data_out <= '1';
				else
					sda_data_out <= serial_slave_addres_rw ;	
				end if;
			when "01" => sda_data_out <= serial_reg_address ;
			when "10" => sda_data_out <= serial_data_to_write ;
			when "11" => sda_data_out <= SDA_AM ;
			when others => sda_data_out <= 'Z';
		end case;
end process;

-- Put a highZ on the SDA line when SDA data out is '1'
-- and place highZ in SDA line and read SDA when I2C bus is not controlled by us
sda_highz_selector : process (SDA_MODE, sda_data_out, SDA)         
	begin                   
		if(SDA_MODE = '0') then
			SDA <= 'Z';
			sda_data_in <= SDA;
		else
			sda_data_in <= sda_data_out;
			if (sda_data_out = '0') then
				SDA <= '0';
			else 
				SDA <= 'Z';
			end if;
      end if;
end process;

-- Replace '1' with highZ on SCL line
scl_highz_selector : process (scl_i)         
	begin                   
		if (scl_i = '0') then
			SCL <= '0';
		else 
			SCL <= 'Z';
		end if;
end process;

-- Register to store SDA ACK/NACK bit
sda_ack_reg : register_n 
					GENERIC MAP(
						N 				=> 1
					)
					PORT MAP(
						RESET 		=> RESET,
						CLK 			=> CLK_IN,
						EN 			=> sda_ack_reg_en_sync,
						DATA_IN(0) 	=> sda_data_in,
						DATA_OUT(0) => SDA_ACK_STORED
					);			

-- SIPO register to parallelize SDA data in
sda_data_in_sipo : sipo_n 
					GENERIC MAP(
						N 			=> 8
					)
					PORT MAP(
						RESET 	=> RESET,
						CLK 		=> CLK_IN,
						EN 		=> sipo_en_sync,
						DATA_IN 	=> sda_data_in,
						DATA_OUT => sda_data_sipo
					);

-- Register to send SDA data in parallelized to sensor controller
sda_data_sipo_reg_out : register_n 
					GENERIC MAP(
						N 				=> 8
					)
					PORT MAP(
						RESET 		=> RESET,
						CLK 			=> CLK_IN,
						EN 			=> REG_OUT_EN,
						DATA_IN 		=> sda_data_sipo,
						DATA_OUT 	=> HR_DATA
					);

-- Counter to count the number of continuous samples done
cnts_samples_counter : counter_up_n 
					GENERIC MAP(
						N				=> 3
					) 					
					PORT MAP(
						RESET 		=> reset_cnts_samples_counter,
						CLK 			=> CLK_IN,
						EN 			=> cnts_samples_counter_en_sync,
						COUNT 		=> samples_count
					);

-- Terminal continuous samples count detector 
cnts_samples_tc_detector : process(samples_count)
	begin
		if (To_integer(Unsigned(samples_count)) = (MAX_NUM_CNTS_SAMPLES-1)) then
			CNTS_SAMPLES_TC <= '1';
		else 
			CNTS_SAMPLES_TC <= '0';
		end if;
end process;

-- Counter to count the 8 bits sent/received by the SDA line
counter_8 : counter_up_n 
					GENERIC MAP(
						N				=> 3
					) 						
					PORT MAP(
						RESET 		=> RESET,
						CLK 			=> CLK_IN,
						EN 			=> count8_en_sync,
						COUNT 		=> count8
					);
-- Detector of the terminal count of the counter of 8 bits
count8_tc_detector : process(count8)
	begin
		if (count8 = "111") then
			COUNT8_TC <= '1';
		else 
			COUNT8_TC <= '0';
		end if;
end process;

-- Counter to select PISO register to be used to put data in SDA line
counter_4 : counter_up_n 
					GENERIC MAP(
						N				=> 2
					) 				
					PORT MAP(
						RESET 		=> count4_counter_reset,
						CLK 			=> CLK_IN,
						EN 			=> count4_en_sync,
						COUNT 		=> COUNT4
					);

-- Counter used to meet I2C constrants between STOP and START conditions
timing_counter : counter_up_n 
					GENERIC MAP(
						N				=> 6
					) 					
					PORT MAP(
						RESET 		=> reset_timing_counter,
						CLK 			=> CLK_IN,
						EN 			=> TIMING_COUNTER_EN,
						COUNT 		=> timing_count
						);

-- Terminal count detector of the timing counter
timing_count_tc_detector : process(timing_count)
	begin
		if (timing_count = "111111") then
			TC_TIMING_WAIT <= '1';
		else 
			TC_TIMING_WAIT <= '0';
		end if;
end process;

-- Toggle bistable that indicates if it is the second slaveID writting
second_addr_flipflop : process(CLK_IN, reset_scl_gen)
	begin
		if CLK_IN'event and CLK_IN = '1' then
			if (reset_scl_gen = '1') then
				second_addr_write_reg_i <= '0';
			elsif (second_addr_write_flip_sync = '1') then
				second_addr_write_reg_i <= not second_addr_write_reg_i;
			end if;
		end if;
end process;

end Behavioral;