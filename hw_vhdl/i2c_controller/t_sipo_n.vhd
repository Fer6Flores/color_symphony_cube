LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY t_sipo_n IS
END t_sipo_n;
 
ARCHITECTURE behavior OF t_sipo_n IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT sipo_n
    PORT(
         RESET : IN  std_logic;
         CLK : IN  std_logic;
         EN : IN  std_logic;
         DATA_IN : IN  std_logic;
         DATA_OUT : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal RESET : std_logic := '0';
   signal CLK : std_logic := '0';
   signal EN : std_logic := '0';
   signal DATA_IN : std_logic := '0';

 	--Outputs
   signal DATA_OUT : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 2500 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: sipo_n PORT MAP (
          RESET => RESET,
          CLK => CLK,
          EN => EN,
          DATA_IN => DATA_IN,
          DATA_OUT => DATA_OUT
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		wait for 100 ns;
		RESET <= '1';
		
      wait for CLK_period*5+CLK_period/2;
		RESET <= '0';

      -- insert stimulus here 
      wait for CLK_period*4;
		EN <= '1';
		DATA_IN <= '1';
		
      wait for CLK_period*4;
		EN <= '0';
		DATA_IN <= '0';

      wait for CLK_period*4+CLK_period/2;
		EN <= '1';
		DATA_IN <= '1';
		
      wait for CLK_period*4;
		EN <= '1';
		DATA_IN <= '1';

      wait;
   end process;

END;
