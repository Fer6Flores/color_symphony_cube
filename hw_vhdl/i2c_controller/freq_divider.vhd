--------------------------------------------------------------
--------------------- Frequency divider ----------------------
--------------------------------------------------------------
-- This frequency divider has a phase resolution of pi/8.
-- Its phase resolution is also down limited to 2*pi/divider.
-- Its max phase value is divider/2.
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity freq_divider is
	Generic ( DIVIDER : INTEGER := 8;
				 PHASE 	: INTEGER := 2);
   Port ( CLOCK_IN 	: in  STD_LOGIC;
           RESET 		: in  STD_LOGIC;
           CLOCK_OUT : out  STD_LOGIC);
end freq_divider;

architecture Behavioral of freq_divider is

-- Signals and constants
constant initial_value 	: INTEGER := DIVIDER * PHASE / 8;

signal count 				: INTEGER range 0 to (DIVIDER/2-1):= initial_value;
signal clock_out_i 		: STD_LOGIC := '1';

begin

-- Assignments
CLOCK_OUT <= clock_out_i;

-- Process
clock_count_update : process(CLOCK_IN)
	begin
		if (CLOCK_IN'event and CLOCK_IN = '1') then
			if RESET = '1' then
				count <= initial_value;
				clock_out_i <= '1';
			else
				count <= count + 1;
				if (count = (DIVIDER/2 - 1)) then
					clock_out_i <= not clock_out_i;
					count <= 0;
				end if;
			end if;
		end if;
end process;

end Behavioral;

