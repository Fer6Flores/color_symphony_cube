--------------------------------------------------------------
----------------------- Control path -------------------------
--------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity control_path is
	 Generic ( MAX_NUM_CNTS_SAMPLES : integer range 0 to 31 := 5);
	 
    Port ( 
				-- GENERAL
				CLK_IN 							: in STD_LOGIC;
				RESET 							: in STD_LOGIC;
			  	
				-- INPUT SIGNALS FROM SENSOR CONTROLLER
				NEW_ORDER	 					: in STD_LOGIC;			
				
				-- OUTPUT SIGNALS TO SENSOR CONTROLLER
				I2C_BUSY 						: out STD_LOGIC;
				ACK_ERROR 						: out STD_LOGIC;
				HR_NEW_DATA 					: out STD_LOGIC;
				
				-- INPUT SIGNALS FROM DATA PATH
				SCL_FD							: in STD_LOGIC;
				SCL_90DEG_FD					: in STD_LOGIC;
				SCL_90DEG_FA					: in STD_LOGIC;
				
				CNTS_SAMPLES_TC 				: in STD_LOGIC;
				COUNT8_TC						: in STD_LOGIC;
				TC_TIMING_WAIT					: in STD_LOGIC;
				WRITING_COUNT					: in STD_LOGIC_VECTOR(1 downto 0);
				
				R_W_STORED						: in  STD_LOGIC;
				CNTS_READ_STORED				: in  STD_LOGIC;
				SDA_ACK_STORED					: in STD_LOGIC;
				SECOND_ADDR_WRITE_REG		: in STD_LOGIC;

				-- OUTPUT SIGNALS TO DATA PATH
				STOP_SCL_90DEG 						: out STD_LOGIC;
				SCL_MODE 								: out STD_LOGIC;
				
				SDA_MODE									: out STD_LOGIC;
				SDA_SELECT 								: out STD_LOGIC_VECTOR(1 downto 0);
				SDA_AM 									: out STD_LOGIC;
				SDA_ACK_REG_EN							: out STD_LOGIC;
				
				PISO_LOAD_EN	 						: out STD_LOGIC;
				SIPO_EN_FSM 							: out STD_LOGIC;
				REG_OUT_EN 								: out STD_LOGIC;
				PISO_EN_FSM 							: out STD_LOGIC;
				
				TIMING_COUNTER_EN						: out STD_LOGIC;
				CNTS_SAMPLES_COUNTER_EN 			: out STD_LOGIC;
				COUNT8_EN_READING						: out STD_LOGIC;
				COUNT4_EN								: out STD_LOGIC;
				CNTS_SAMPLES_COUNTER_RESET_FSM 	: out STD_LOGIC;
				WRITING_COUNTER_RESET				: out STD_LOGIC;
				SECOND_ADDR_WRITE_FLIP				: out STD_LOGIC
				);
				
end control_path;

architecture Behavioral of control_path is

-- State names
type state_type is (	s0_wait, s1_load_command, s2_start, s3_end_start_condition, s4_write_data, 
							s5_let_i2c_be_free, s6_rep_start, s7_end_rep_start, s8_wait_one_scl_to_read, 
							s9_read_byte, s10_update_data_out, s11_gen_am, s12_gen_nack, s13_stop, 
							s14_end_stop, s15_wait_timing); 
signal state, next_state : state_type; 

-- Output internal signals to sensor controller
signal i2c_busy_i 		: STD_LOGIC := '0';
signal ack_error_i 		: STD_LOGIC := '0';
signal hr_new_data_i		: STD_LOGIC := '0';

-- Data path internal signals
signal stop_scl_90deg_i 						: STD_LOGIC := '0';
signal scl_mode_i 								: STD_LOGIC := '0';

signal sda_mode_i									: STD_LOGIC := '0'; 	-- Zero value means SDA 'Z' value
signal sda_select_i 								: STD_LOGIC_VECTOR(1 downto 0) := (others => '0');
signal sda_am_i 									: STD_LOGIC := '1';
signal sda_ack_reg_en_i							: STD_LOGIC := '0';

signal piso_load_en_i							: STD_LOGIC := '0';
signal sipo_en_fsm_i 							: STD_LOGIC := '0';
signal reg_out_en_i 								: STD_LOGIC := '0';
signal piso_en_fsm_i 							: STD_LOGIC := '0';

signal timing_counter_en_i			 			: STD_LOGIC := '0';
signal cnts_samples_counter_en_i 			: STD_LOGIC := '0';
signal count8_en_reading_i						: STD_LOGIC := '0';
signal count4_en_i					 			: STD_LOGIC := '0';
signal cnts_samples_counter_reset_fsm_i	: STD_LOGIC := '0';
signal writing_counter_reset_i 				: STD_LOGIC := '0';

signal second_addr_write_flip_i 				: STD_LOGIC := '0';

begin
 
-- Process

-- Synchronization of state update
SYNC_UPDATE_STATE : process(CLK_IN)
	begin
		if (CLK_IN'event and CLK_IN = '1') then
			if (RESET = '1') then
				state <= s0_wait;
			else
				state <= next_state;
			end if;
		end if;
end process;

-- Output signals update
UPDATE_OUTPUTS : process (	state, reset, piso_load_en_i, stop_scl_90deg_i, scl_mode_i, sipo_en_fsm_i, 
									piso_en_fsm_i, reg_out_en_i, cnts_samples_counter_en_i, count4_en_i, 
									sda_select_i, sda_ack_reg_en_i, count8_en_reading_i, sda_am_i, i2c_busy_i, 
									cnts_samples_counter_reset_fsm_i, second_addr_write_flip_i, hr_new_data_i, 
									writing_counter_reset_i, ack_error_i, sda_mode_i, timing_counter_en_i)
	begin
		if (RESET = '1') then				
			-- DATA PATH ENABLES RESET
			PISO_LOAD_EN 							<= '0';
			SCL_MODE		 							<= '1';	-- Stop SCL when reset high
			STOP_SCL_90DEG 						<= '1'; 	-- Stop SCL with pahse of 90� when reset high
			SIPO_EN_FSM 							<= '0';
			PISO_EN_FSM 							<= '0';
			REG_OUT_EN 								<= '0';
			TIMING_COUNTER_EN			 			<= '0';
			CNTS_SAMPLES_COUNTER_EN 			<= '0';
			COUNT4_EN								<= '0';
			SDA_SELECT								<= "11"; -- SDA mux select the control path signal
			SDA_ACK_REG_EN							<= '0';
			COUNT8_EN_READING						<= '0';
			CNTS_SAMPLES_COUNTER_RESET_FSM 	<= '0';	
			SECOND_ADDR_WRITE_FLIP				<=	'0';
			WRITING_COUNTER_RESET				<=	'0';
			
			-- DATA AND I2C STATE COMMUNICATION RESET
			SDA_AM 			<= '1';	-- Let highZ SDA when reset high
			I2C_BUSY 		<= '0';
			ACK_ERROR 		<= '0';
			HR_NEW_DATA 	<= '0';
			SDA_MODE 		<= '0';
			
		else		
			-- DATA PATH ENABLES UPDATE
			PISO_LOAD_EN	 						<= piso_load_en_i;
			STOP_SCL_90DEG 						<= stop_scl_90deg_i;
			SCL_MODE 								<= scl_mode_i;
			SIPO_EN_FSM 							<= sipo_en_fsm_i;
			PISO_EN_FSM 							<= piso_en_fsm_i;
			REG_OUT_EN 								<= reg_out_en_i;
			TIMING_COUNTER_EN			 			<= timing_counter_en_i;
			CNTS_SAMPLES_COUNTER_EN 			<= cnts_samples_counter_en_i;
			COUNT4_EN								<= count4_en_i;
			SDA_SELECT								<= sda_select_i;
			SDA_ACK_REG_EN							<= sda_ack_reg_en_i;
			COUNT8_EN_READING						<= count8_en_reading_i;
			CNTS_SAMPLES_COUNTER_RESET_FSM 	<= cnts_samples_counter_reset_fsm_i;
			SECOND_ADDR_WRITE_FLIP				<=	second_addr_write_flip_i;
			WRITING_COUNTER_RESET				<=	writing_counter_reset_i;

			-- DATA AND I2C STATE COMMUNICATION UPDATE
			SDA_AM 			<= sda_am_i;
			I2C_BUSY 		<= i2c_busy_i;
			ACK_ERROR 		<= ack_error_i;
			HR_NEW_DATA 	<= hr_new_data_i;
			SDA_MODE 		<= sda_mode_i;
		end if;        
end process;

-- Moore State-Machine output signals decode
OUTPUT_DECODE: process (state, WRITING_COUNT, SDA_ACK_STORED)
begin

	-- DATA PATH ENABLES DEFAULT VALUES
	piso_load_en_i 						<= '0';
	stop_scl_90deg_i 						<= '0';	-- By default, SCL with a initial phase of 90� is running
	scl_mode_i 								<= '0';	-- By default, SCL is running
	sipo_en_fsm_i 							<= '0';
	piso_en_fsm_i 							<= '0';
	reg_out_en_i 							<= '0';
	timing_counter_en_i					<= '0';
	cnts_samples_counter_en_i 			<= '0';
	count4_en_i								<= '0';
	sda_select_i							<= "00";
	sda_ack_reg_en_i						<= '0';
	count8_en_reading_i					<= '0';
	cnts_samples_counter_reset_fsm_i <= '0';
	second_addr_write_flip_i 			<= '0';
	writing_counter_reset_i				<= '0';
	second_addr_write_flip_i 			<= '0';

	-- DATA AND I2C STATE COMMUNICATION DEFAULT VALUES
	sda_am_i 		<= '0';
	i2c_busy_i 		<= '0';
	ack_error_i 	<= '0';
	hr_new_data_i 	<= '0';
	sda_mode_i 		<= '0';
	
	case (state) is
		
		-- Waiting for a new I2C communication process request
		when s0_wait => 
			stop_scl_90deg_i 						<= '1'; 	-- Stop SCL in high value with a phase of 90� when waiting
			scl_mode_i								<= '1'; 	-- Stop SCL in high value when waiting
			i2c_busy_i 								<= '0';  -- Notify that we are not busy. (This assignment it is not necesary)
			cnts_samples_counter_reset_fsm_i <= '1'; 	-- Kill this counter when a I2C communication process ends
			writing_counter_reset_i				<= '1';	-- Kill the counter that chooses the PISO that write SDA
		
		-- Store the data povided by the sensor controller
		when s1_load_command =>
			stop_scl_90deg_i 	<= '1'; 		-- Stop SCL in high value with a phase of 90� when waiting
			scl_mode_i			<= '1'; 		-- Stop SCL in high value when waiting
			sda_mode_i 			<= '1'; 		-- Let's take the control of the I2C bus
			sda_select_i 		<= "11";		-- SDA will be written by the signal sda_am_i of this control path
			sda_am_i 			<= '1';		-- Put a 'Z' in SDA to do the start condition
			i2c_busy_i 			<= '1';		-- Set the I2C_busy signal to notify that we are busy
			piso_load_en_i 	<= '1';		-- We take the data provided by the sensor controller
		
		-- Start the start condition (woah, that joke)
		when s2_start => 
			sda_mode_i 			<= '1'; 		-- Let's take the control of the I2C bus
			sda_select_i 		<= "11";		-- SDA will be written by the signal sda_am_i of this control path
			sda_am_i 			<= '1';		-- Put a 'Z' in SDA to do the start condition
			i2c_busy_i 			<= '1';		-- Set the I2C_busy signal to notify that we are busy
		
		-- End the start condition (Clear SDA)
		when s3_end_start_condition =>
			sda_mode_i 			<= '1'; 		-- Let's take the control of the I2C bus
			sda_select_i 		<= "11";		-- SDA will be written by the signal sda_am_i of this control path
			sda_am_i 			<= '0';		-- Put a '0' in SDA to end the start condition
			i2c_busy_i 			<= '1';		-- Set the I2C_busy signal to notify that we are busy
		
		-- Write a data byte in SDA		
		when s4_write_data =>
			sda_mode_i 			<= '1'; 				-- Let's keep the control of the I2C bus
			sda_select_i 		<= WRITING_COUNT;	-- SDA will be written by the signal PISO register choosen by WRITING_COUNT
			i2c_busy_i 			<= '1';				-- Set the I2C_busy signal to notify that we are busy
			piso_en_fsm_i 		<= '1';				-- Every SCL_90DEG_FA the SDA value will be updated
			
		-- Let I2C be free and store ACK/NACK value from slave
		when s5_let_i2c_be_free =>
			i2c_busy_i 			<= '1';				-- Set the I2C_busy signal to notify that we are busy
			count4_en_i 		<= '1';				-- Add 1 to WRITING_COUNT
			sda_ack_reg_en_i 	<= '1';				-- Store ACK/NACK value from slave when SCL_90DEG_FD
		
		-- Begin the repeated start condition (SDA <= 'Z')	
		when s6_rep_start => 
			i2c_busy_i 						<= '1';	-- Set the I2C_busy signal to notify that we are busy
			sda_mode_i 						<= '1';	-- Take again the I2C bus control
			sda_select_i 					<= "11";	-- SDA will be written by the signal sda_am_i of this control path
			sda_am_i 						<= '1';	-- Set SDA to start the repeated start condition
			writing_counter_reset_i		<= '1';	-- Get WRITING_COUNT to the value correspondent to the slaveID PISO register
			second_addr_write_flip_i 	<= '1';	-- Set a Toggle biestable to indicate that is the second slaveID written
			
		-- End the repeated start condition (SDA <= '0')
		when s7_end_rep_start => 
			sda_mode_i 			<= '1'; 		-- Let's keep the control of the I2C bus
			sda_select_i 		<= "11";		-- SDA will be written by the signal sda_am_i of this control path
			sda_am_i 			<= '0';		-- Put a '0' in SDA to end the repeated start condition
			i2c_busy_i 			<= '1';		-- Set the I2C_busy signal to notify that we are busy
		
		-- Take the first bit of the data byte from SDA, but stop the counter8
		when s8_wait_one_scl_to_read =>
			i2c_busy_i 						<= '1';	-- Set the I2C_busy signal to notify that we are busy
			second_addr_write_flip_i	<= '1';	-- Clear the Toggle biestable
			sipo_en_fsm_i 					<= '1'; 	-- Store SDA data in a SIPO register in the middle of high SCL
		
		-- Continue reading the data byte from SDA and enable the counter8
		when s9_read_byte => 
			i2c_busy_i 						<= '1';	-- Set the I2C_busy signal to notify that we are busy
			count8_en_reading_i 			<= '1';	-- Enable the counter8 
			sipo_en_fsm_i 					<= '1'; 	-- Store SDA data in a SIPO register in the middle of high SCL
						
		-- Update the data output to the sensor controller
		when s10_update_data_out =>
			i2c_busy_i 						<= '1';	-- Set the I2C_busy signal to notify that we are busy
			reg_out_en_i 					<= '1';	-- Enable the data output register 
						
		-- Generate one ACK from master (AM) to end the reading
		when s11_gen_am => 
			i2c_busy_i 						<= '1';	-- Set the I2C_busy signal to notify that we are busy
			sda_mode_i 						<= '1'; 	-- Let's keep the control of the I2C bus
			sda_select_i 					<= "11";	-- SDA will be written by the signal sda_am_i of this control path
			sda_am_i 						<= '0';	-- Put a '0' in SDA to generate the AM. (Assignment not necessary)
			count8_en_reading_i 			<= '1';	-- Enable the counter8 each SCL_90DEG_FD
			hr_new_data_i 					<= '1';	-- Notify the available new data byte
			cnts_samples_counter_en_i 	<= '1';	-- Increase the continuous samples count
							
		-- Generate a NACK from master to continue the reading 
		when s12_gen_nack =>
			i2c_busy_i 						<= '1';	-- Set the I2C_busy signal to notify that we are busy
			sda_mode_i 						<= '1'; 	-- Let's keep the control of the I2C bus
			sda_select_i 					<= "11";	-- SDA will be written by the signal sda_am_i of this control path
			sda_am_i 						<= '1';	-- Put a 'Z' in SDA to generate the NACK.
			count8_en_reading_i 			<= '1';	-- Enable the counter8 each SCL_90DEG_FD
			hr_new_data_i 					<= '1';	-- Notify the available new data byte
		
		-- The begin of the end (of the I2C universe)
		when s13_stop => 
			i2c_busy_i 								<= '1';					-- Set the I2C_busy signal to notify that we are busy
			sda_mode_i 								<= '1'; 					-- Let's keep the control of the I2C bus
			sda_select_i 							<= "11";					-- SDA will be written by the signal sda_am_i of this control path
			sda_am_i 								<= '0';					-- Put a '0' in SDA to start the stop condition
			cnts_samples_counter_reset_fsm_i <= '1'; 					-- Kill the counter of continuous samples
			writing_counter_reset_i				<= '1';					-- Reset again the counter of the PISOs selector
			ack_error_i 							<= SDA_ACK_STORED; 	-- Notify if an error has happened
		
		-- Finish the stop condition
		when s14_end_stop =>
			i2c_busy_i 			<= '1';		-- Set the I2C_busy signal to notify that we are busy
			sda_mode_i 			<= '1'; 		-- Let's keep the control of the I2C bus
			sda_select_i 		<= "11";		-- SDA will be written by the signal sda_am_i of this control path
			sda_am_i 			<= '1';		-- Put a 'Z' in SDA to generate the stop condition.
			scl_mode_i			<= '1';		-- Stop the SCL clock
		
		-- Waiting state to meet I2C timing constraints
		when s15_wait_timing => 
			timing_counter_en_i					<= '1';
			i2c_busy_i 								<= '1';	-- Set the I2C_busy signal to notify that we are busy
			stop_scl_90deg_i 						<= '1'; 	-- Stop SCL in high value with a phase of 90� when waiting
			scl_mode_i								<= '1'; 	-- Stop SCL in high value when waiting
			cnts_samples_counter_reset_fsm_i <= '1'; 	-- Kill this counter when a I2C communication process ends
			writing_counter_reset_i				<= '1';	-- Kill the counter that chooses the PISO that write SDA

		-- Por si las moscas
		when others =>
			stop_scl_90deg_i 		<= '1';		-- Stop SCL in high value with a phase of 90� when waiting
			scl_mode_i				<= '1';		-- Stop SCL in high value when waiting

	end case;
end process;

-- State-Machine transitions decode
NEXT_STATE_DECODE: process (	state, NEW_ORDER, SCL_FD, SCL_90DEG_FD, SCL_90DEG_FA, COUNT8_TC, 
										CNTS_SAMPLES_TC, R_W_STORED, SECOND_ADDR_WRITE_REG, SDA_ACK_STORED, 
										CNTS_READ_STORED, WRITING_COUNT, TC_TIMING_WAIT)
begin

	next_state <= state;  -- Default is to stay in current state
	
	case state is
	
		when s0_wait => 
			if (NEW_ORDER = '1') then
				next_state <= s1_load_command;				-- If the sensor controller do a new request take the data from it
			end if;
		
		when s1_load_command =>
			next_state <= s2_start;								-- Start the I2C process
			
		when s2_start => 
			if (SCL_90DEG_FD = '1') then
				next_state <= s3_end_start_condition;		-- If start condition first part has ended, go finish it
			end if;			
			
		when s3_end_start_condition => 
			if (SCL_90DEG_FA = '1') then
				next_state <= s4_write_data;					-- When start condition ends, start write slaveID
			end if;
			
		when s4_write_data => 
			if ((COUNT8_TC = '1') and (SCL_90DEG_FA = '1')) then
				next_state <= s5_let_i2c_be_free;			-- If data byte writing ends, get ready to received ACK/NACK
			end if;

		when s5_let_i2c_be_free => 
			if (SCL_90DEG_FA = '1') then
				if (SDA_ACK_STORED = '1') then
					next_state <= s13_stop;						-- If a NACK is received, ends transmission and notify the error
					
				elsif ((R_W_STORED = '1') and (WRITING_COUNT = "10")) then
					next_state <= s6_rep_start;				-- If slaveID, register address has been written and it is a reading, 
																		-- do a repeated start
																		
				elsif ((writing_count = "11") and (SECOND_ADDR_WRITE_REG = '0')) then
					next_state <= s13_stop;						-- If slaveID, register addres, and a data byte has been written,
																		-- end the I2C communication
																		
				elsif (SECOND_ADDR_WRITE_REG = '1') then
					next_state <= s8_wait_one_scl_to_read;	-- If the second slaveID has been written, start the reading
				else
				
					next_state <= s4_write_data;				-- If only the slaveID has been written or it is a writing process that
																		-- has not ended, continue writind data bytes
				end if;
			end if;

		when s6_rep_start => 
			if (SCL_90DEG_FD = '1') then
				next_state <= s7_end_rep_start;				-- If a repeated start has begin, end it
			end if;
	
		when s7_end_rep_start => 
			if (SCL_90DEG_FA = '1') then
				next_state <= s4_write_data;					-- If a repeated start has ended, write the slaveID again
			end if;
			
		when s8_wait_one_scl_to_read =>
			if (SCL_FD = '1') then
				next_state <= s9_read_byte;					-- If all writings okey in a reading process, start reading
			end if;

		when s9_read_byte => 
			if ((COUNT8_TC = '1') and (SCL_90DEG_FA = '1')) then
					next_state <= s10_update_data_out;		-- When a byte is read, update the output data and notify it
			end if;
		
		when s10_update_data_out =>
				if (CNTS_SAMPLES_TC = '0') and (CNTS_READ_STORED = '1') then
					next_state <= s11_gen_am;					-- If the continuous samples has not ended, do an AM and read more
				else
					next_state <= s12_gen_nack;				-- If the reading process has ended, generate a NACK
				end if;
				
		when s11_gen_am => 
			if (SCL_90DEG_FA = '1') then
				next_state <= s8_wait_one_scl_to_read;		-- If the AM has been generated, continue reading
			end if;

		when s12_gen_nack => 
			if (SCL_90DEG_FA = '1') then
				next_state <= s13_stop;							-- If the NACK has been generated, do a stop condition
			end if;
		
		when s13_stop => 
			if (SCL_90DEG_FD = '1') then
				next_state <= s14_end_stop;					-- If the end is near, finish the stop condition
			end if;
			
		when s14_end_stop =>
			if (SCL_90DEG_FA = '1') then
				next_state <= s15_wait_timing;				-- If the stop condition has ended, go wait more work
			end if;
		
		when s15_wait_timing => 
			if (TC_TIMING_WAIT = '1') then
				next_state <= s0_wait;							-- When I2C timing constraint is ready, 
																		-- notify that i2c controller is free
			end if;
			
		when others =>
			next_state <= s0_wait;								-- Ante la duda, espera sentado
	end case;
	
end process;

end Behavioral;

