--------------------------------------------------------------
-------------------- I2C bus controller ----------------------
--------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity i2c_controller is

	 Generic ( MAX_NUM_I2C_CNTS_SAMPLES : integer range 0 to 31 := 2);
	 
    Port ( 
				-- GENERAL
				CLK_IN 				: in STD_LOGIC;
				CLK_I2C_AUX_3200K : in STD_LOGIC;
				RESET 				: in STD_LOGIC;
				
				-- I2C BUS
				SDA 					: inout STD_LOGIC;
				SCL 					: inout STD_LOGIC;
			  
				-- INPUT SIGNALS FROM SENSOR CONTROLLER
				SLAVE_ADDRESS 		: in STD_LOGIC_VECTOR (6 downto 0);
				R_W 					: in STD_LOGIC;
				REG_ADDRESS 		: in STD_LOGIC_VECTOR (7 downto 0);
				DATA_TO_WRITE 		: in STD_LOGIC_VECTOR (7 downto 0);
				NEW_ORDER 			: in STD_LOGIC;
				CNTS_READ 			: in STD_LOGIC;
			  
				-- OUTPUT SIGNALS FROM SENSOR CONTROLLER
				I2C_BUSY 			: out STD_LOGIC;
				ACK_ERROR 			: out STD_LOGIC;
				HR_NEW_DATA 		: out STD_LOGIC;
				HR_DATA 				: out STD_LOGIC_VECTOR (7 downto 0)
				);		  
end i2c_controller;

architecture Behavioral of i2c_controller is

-- Signals from control path to data path
signal stop_scl_90deg_i							: STD_LOGIC := '1';
signal scl_mode_i									: STD_LOGIC := '1';
signal sda_mode_i 								: STD_LOGIC := '0';
signal sda_select_i 								: STD_LOGIC_VECTOR(1 downto 0) := "00";
signal sda_am_i 									: STD_LOGIC := '0';
signal sda_ack_reg_en_i 						: STD_LOGIC := '0';
signal piso_load_en_i 							: STD_LOGIC := '0';
signal sipo_en_fsm_i 							: STD_LOGIC := '0';
signal reg_out_en_i 								: STD_LOGIC := '0';
signal piso_en_fsm_i 							: STD_LOGIC := '0';
signal cnts_samples_counter_en_i 			: STD_LOGIC := '0';
signal timing_counter_en_i			 			: STD_LOGIC := '0';
signal count8_en_reading_i 					: STD_LOGIC := '0';
signal count4_en_i 								: STD_LOGIC := '0';
signal cnts_samples_counter_reset_fsm_i	: STD_LOGIC := '0';
signal writing_counter_reset_i				: STD_LOGIC := '0';
signal second_addr_write_flip_i				: STD_LOGIC := '0';

-- Signals from data path to control path
signal scl_fd_i						: STD_LOGIC := '0';
signal scl_90deg_fd_i 				: STD_LOGIC := '0';
signal scl_90deg_fa_i 				: STD_LOGIC := '0';
signal sda_ack_stored_i 			: STD_LOGIC := '0';
signal tc_timing_wait_i 			: STD_LOGIC := '0';
signal cnts_samples_tc_i 			: STD_LOGIC := '0';
signal count8_tc_i 					: STD_LOGIC := '0';
signal count4_i 						: STD_LOGIC_VECTOR(1 downto 0) := "00";
signal r_w_stored_i 					: STD_LOGIC := '0';
signal cnts_read_stored_i 			: STD_LOGIC := '0';
signal second_addr_write_reg_i 	: STD_LOGIC := '0';

-- Components 
component control_path

	 Generic ( MAX_NUM_CNTS_SAMPLES 	: integer range 0 to 31 := 5);
	 
    Port ( 
				-- GENERAL
				CLK_IN 						: in STD_LOGIC;
				RESET 						: in STD_LOGIC;
			  	
				-- INPUT SIGNALS FROM SENSOR CONTROLLER
				NEW_ORDER	 				: in STD_LOGIC;
							
				-- OUTPUT SIGNALS TO SENSOR CONTROLLER
				I2C_BUSY 					: out STD_LOGIC;
				ACK_ERROR 					: out STD_LOGIC;
				HR_NEW_DATA 				: out STD_LOGIC;
				
				-- INPUT SIGNALS FROM DATA PATH
				SCL_FD						: in STD_LOGIC;
				SCL_90DEG_FD				: in STD_LOGIC;
				SCL_90DEG_FA				: in STD_LOGIC;
				
				CNTS_SAMPLES_TC 			: in STD_LOGIC;
				COUNT8_TC					: in STD_LOGIC;
				TC_TIMING_WAIT				: in STD_LOGIC;
				WRITING_COUNT				: in STD_LOGIC_VECTOR(1 downto 0);
				
				R_W_STORED					: in STD_LOGIC;
				CNTS_READ_STORED			: in STD_LOGIC;
				SDA_ACK_STORED				: in STD_LOGIC;
				SECOND_ADDR_WRITE_REG	: in STD_LOGIC;	

				
				-- OOUTPUT SIGNALS TO DATA PATH
				STOP_SCL_90DEG							: out STD_LOGIC;
				SCL_MODE 								: out STD_LOGIC;
				
				SDA_MODE									: out STD_LOGIC;
				SDA_SELECT 								: out STD_LOGIC_VECTOR(1 downto 0);
				SDA_AM 									: out STD_LOGIC;
				SDA_ACK_REG_EN							: out STD_LOGIC;
				
				PISO_LOAD_EN	 						: out STD_LOGIC;
				SIPO_EN_FSM 							: out STD_LOGIC;
				REG_OUT_EN 								: out STD_LOGIC;
				PISO_EN_FSM 							: out STD_LOGIC;
				
				TIMING_COUNTER_EN						: out STD_LOGIC;
				CNTS_SAMPLES_COUNTER_EN 			: out STD_LOGIC;
				COUNT8_EN_READING						: out STD_LOGIC;
				COUNT4_EN								: out STD_LOGIC;
				CNTS_SAMPLES_COUNTER_RESET_FSM 	: out STD_LOGIC;
				WRITING_COUNTER_RESET				: out STD_LOGIC;
				
				SECOND_ADDR_WRITE_FLIP				: out STD_LOGIC
				);
end component;

COMPONENT data_path
	 Generic ( MAX_NUM_CNTS_SAMPLES : integer range 0 to 31 := 5); -- ver si mejorar esto a una se�al variable
	 
    Port ( 
				-- GENERAL
				CLK_IN 				: in STD_LOGIC;
				CLK_I2C_AUX_3200K : in STD_LOGIC;
				RESET 				: in STD_LOGIC;
				
				-- I2C BUS
				SDA					: inout STD_LOGIC;
				SCL 					: inout STD_LOGIC;
				
				-- INPUT SIGNALS TO SENSOR CONTROLLER
				SLAVE_ADDRESS 		: in STD_LOGIC_VECTOR (6 downto 0);
				R_W 					: in STD_LOGIC;
				REG_ADDRESS 		: in STD_LOGIC_VECTOR (7 downto 0);
				DATA_TO_WRITE 		: in STD_LOGIC_VECTOR (7 downto 0);
				CNTS_READ 			: in STD_LOGIC;
			
				-- OUTPUT SIGNALS TO SENSOR CONTROLLER
				HR_DATA 				: out  STD_LOGIC_VECTOR (7 downto 0);
				
				-- INPUT SIGNALS FROM CONTROL PATH
				NEW_START	 							: in STD_LOGIC;
				STOP_SCL_90DEG							: in STD_LOGIC;
				SCL_MODE 								: in STD_LOGIC;
				SDA_MODE									: in STD_LOGIC;
				SDA_SELECT 								: in STD_LOGIC_VECTOR(1 downto 0);
				SDA_AM 									: in STD_LOGIC;
				SDA_ACK_REG_EN							: in STD_LOGIC;
				CNTS_SAMPLES_COUNTER_EN 			: in STD_LOGIC;
				COUNT8_EN_READING						: in STD_LOGIC;
				COUNT4_EN								: in STD_LOGIC;
				CNTS_SAMPLES_COUNTER_RESET_FSM 	: in STD_LOGIC;
				SIPO_EN_FSM 							: in STD_LOGIC;
				REG_OUT_EN 								: in STD_LOGIC;
				PISO_EN_FSM 							: in STD_LOGIC;
				SECOND_ADDR_WRITE_FLIP				: in STD_LOGIC;
				WRITING_COUNTER_RESET				: in STD_LOGIC;
				TIMING_COUNTER_EN						: in STD_LOGIC;

				-- OUTPUT SIGNALS TO CONTROL PATH
				SCL_FD									: out STD_LOGIC;
				SCL_90DEG_FD							: out STD_LOGIC;
				SCL_90DEG_FA							: out STD_LOGIC;
				TC_TIMING_WAIT							: out STD_LOGIC;
				SDA_ACK_STORED							: out STD_LOGIC;
				CNTS_SAMPLES_TC 						: out STD_LOGIC;
				COUNT8_TC								: out STD_LOGIC;	
				COUNT4									: out STD_LOGIC_VECTOR(1 downto 0);
				R_W_STORED								: out STD_LOGIC;
				CNTS_READ_STORED						: out STD_LOGIC;
				SECOND_ADDR_WRITE_REG				: out STD_LOGIC	
				);
end COMPONENT;

begin

-- Instances				
the_brain : control_path 
		GENERIC MAP( 
			MAX_NUM_CNTS_SAMPLES => MAX_NUM_I2C_CNTS_SAMPLES
		)
		PORT MAP(
			CLK_IN 				=> CLK_IN,				-- [from] Nexys 3
			RESET 				=> RESET,				-- [from] Nexys 3
			
			NEW_ORDER	 		=> NEW_ORDER,			-- [from] Sensor controller
			
			I2C_BUSY 			=> I2C_BUSY,			-- [to]	 Sensor controller
			ACK_ERROR 			=> ACK_ERROR,			-- [to]	 Sensor controller
			HR_NEW_DATA 		=> HR_NEW_DATA,		-- [to] 	 Sensor controller
			
			SCL_FD 						=> scl_fd_i,						-- [from] Data path 
			SCL_90DEG_FD 				=> scl_90deg_fd_i,				-- [from] Data path
			SCL_90DEG_FA 				=> scl_90deg_fa_i,				-- [from] Data path
			CNTS_SAMPLES_TC 			=> cnts_samples_tc_i,			-- [from] Data path
			COUNT8_TC 					=> count8_tc_i,					-- [from] Data path
			TC_TIMING_WAIT				=> tc_timing_wait_i,				-- [from] Data path
			WRITING_COUNT 				=> count4_i,						-- [from] Data path
			R_W_STORED 					=> r_w_stored_i,					-- [from] Data path
			CNTS_READ_STORED 			=> cnts_read_stored_i,			-- [from] Data path
			SDA_ACK_STORED 			=> sda_ack_stored_i,				-- [from] Data path
			SECOND_ADDR_WRITE_REG	=> second_addr_write_reg_i,	-- [from] Data path
			
			STOP_SCL_90DEG 						=> stop_scl_90deg_i,							-- [to] Data path
			SCL_MODE 								=> scl_mode_i,									-- [to] Data path
			SDA_MODE 								=> sda_mode_i,									-- [to] Data path
			SDA_SELECT 								=> sda_select_i,								-- [to] Data path
			SDA_AM 									=> sda_am_i,									-- [to] Data path
			SDA_ACK_REG_EN 						=> sda_ack_reg_en_i,							-- [to] Data path
			PISO_LOAD_EN 							=> piso_load_en_i,							-- [to] Data path
			SIPO_EN_FSM 							=> sipo_en_fsm_i,								-- [to] Data path
			REG_OUT_EN 								=> reg_out_en_i,								-- [to] Data path
			PISO_EN_FSM 							=> piso_en_fsm_i,								-- [to] Data path
			TIMING_COUNTER_EN						=> timing_counter_en_i,						-- [to] Data path
			CNTS_SAMPLES_COUNTER_EN 			=> cnts_samples_counter_en_i,				-- [to] Data path
			COUNT8_EN_READING 					=> count8_en_reading_i,						-- [to] Data path	
			COUNT4_EN 								=> count4_en_i,								-- [to] Data path
			CNTS_SAMPLES_COUNTER_RESET_FSM 	=> cnts_samples_counter_reset_fsm_i,	-- [to] Data path
			SECOND_ADDR_WRITE_FLIP				=> second_addr_write_flip_i,				-- [to] Data path
			WRITING_COUNTER_RESET				=> writing_counter_reset_i					-- [to] Data path
		);

the_body: data_path 
		GENERIC MAP( 
			MAX_NUM_CNTS_SAMPLES => MAX_NUM_I2C_CNTS_SAMPLES
		)
		PORT MAP(
			CLK_IN 				=> CLK_IN,					-- [from] Nexys 3
			CLK_I2C_AUX_3200K => CLK_I2C_AUX_3200K,	-- [from] Nexys 3 (DCM)
			RESET 				=> RESET,					-- [from] Nexys 3
			
			SDA 					=> SDA,						-- [to]	 I2C bus
			SCL 					=> SCL,						-- [to]	 I2C bus
			
			SLAVE_ADDRESS 		=> SLAVE_ADDRESS,			-- [from] Sensor controller
			R_W 					=> R_W,						-- [from] Sensor controller
			REG_ADDRESS 		=> REG_ADDRESS,			-- [from] Sensor controller
			DATA_TO_WRITE 		=> DATA_TO_WRITE,			-- [from] Sensor controller
			CNTS_READ 			=> CNTS_READ,				-- [from] Sensor controller

			HR_DATA 				=> HR_DATA,					-- [to]	 Sensor controller
			
			NEW_START 								=> piso_load_en_i,							-- [from] Control path	
			STOP_SCL_90DEG 						=> stop_scl_90deg_i,							-- [from] Control path
			SCL_MODE 								=> scl_mode_i,									-- [from] Control path
			SDA_MODE 								=> sda_mode_i,									-- [from] Control path
			SDA_SELECT 								=> sda_select_i,								-- [from] Control path
			SDA_AM 									=> sda_am_i,									-- [from] Control path
			SDA_ACK_REG_EN							=> sda_ack_reg_en_i,							-- [from] Control path
			CNTS_SAMPLES_COUNTER_EN 			=> cnts_samples_counter_en_i,				-- [from] Control path
			COUNT8_EN_READING 					=> count8_en_reading_i,						-- [from] Control path
			COUNT4_EN 								=> count4_en_i,								-- [from] Control path
			CNTS_SAMPLES_COUNTER_RESET_FSM 	=> cnts_samples_counter_reset_fsm_i,	-- [from] Control path
			WRITING_COUNTER_RESET				=> writing_counter_reset_i,				-- [from] Control path
			SIPO_EN_FSM 							=> sipo_en_fsm_i,								-- [from] Control path
			REG_OUT_EN 								=> reg_out_en_i,								-- [from] Control path
			PISO_EN_FSM 							=> piso_en_fsm_i,								-- [from] Control path
			SECOND_ADDR_WRITE_FLIP				=> second_addr_write_flip_i,				-- [from] Control path
			TIMING_COUNTER_EN						=> timing_counter_en_i,
			
			SCL_FD 						=> scl_fd_i,						-- [to]	 Control path
			SCL_90DEG_FD				=> scl_90deg_fd_i,				-- [to]	 Control path
			SCL_90DEG_FA				=> scl_90deg_fa_i,				-- [to]	 Control path
			TC_TIMING_WAIT				=> tc_timing_wait_i,
			SDA_ACK_STORED 			=> sda_ack_stored_i,				-- [to]	 Control path
			CNTS_SAMPLES_TC 			=> cnts_samples_tc_i,			-- [to]	 Control path
			COUNT8_TC 					=> count8_tc_i,					-- [to]	 Control path
			COUNT4 						=> count4_i,						-- [to]	 Control path
			R_W_STORED 					=> r_w_stored_i,					-- [to]	 Control path
			CNTS_READ_STORED 			=> cnts_read_stored_i,			-- [to]	 Control path
			SECOND_ADDR_WRITE_REG	=> second_addr_write_reg_i		-- [to]	 Control path
		);

end Behavioral;