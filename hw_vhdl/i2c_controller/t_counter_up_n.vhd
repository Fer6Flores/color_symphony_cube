LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY t_counter_up_n IS
END t_counter_up_n;
 
ARCHITECTURE behavior OF t_counter_up_n IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT counter_up_n
    PORT(
         RESET : IN  std_logic;
         CLK : IN  std_logic;
         EN : IN  std_logic;
         COUNT : OUT  std_logic_vector(0 to 4)
        );
    END COMPONENT;
    

   --Inputs
   signal RESET : std_logic := '0';
   signal CLK : std_logic := '0';
   signal EN : std_logic := '0';

 	--Outputs
   signal COUNT : std_logic_vector(0 to 4);

   -- Clock period definitions
   constant CLK_period : time := 2500 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: counter_up_n PORT MAP (
          RESET => RESET,
          CLK => CLK,
          EN => EN,
          COUNT => COUNT
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		wait for 100 ns;
		RESET <= '1';
		
      wait for CLK_period*10;
		RESET <= '0';
		
      -- insert stimulus here 
		wait for CLK_period*4+CLK_period/2;
		EN <= '1';
		
		wait for CLK_period*4;
		EN <= '0';
		
		wait for CLK_period*4+CLK_period/2;
		EN <= '1';
		
		wait for CLK_period*4;
		reset <= '1';
		
		wait for CLK_period;
		reset <= '0';
		EN <= '1';

      wait;
   end process;

END;
