--------------------------------------------------------------
---------------------- Edge detector -------------------------
--------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity edge_detector is
	port(	clk			: in std_logic;
			reset			: in std_logic;
			pulse			: in std_logic;
			fa_pulse		: out std_logic;
			fd_pulse		: out std_logic
		);
end edge_detector;

architecture Behavioral of edge_detector is

begin
	process(clk,reset) is
	variable xi_t, xi_t_1 : std_logic;
	begin		
		if rising_edge(clk) then 				
			if reset='1' then							
				xi_t := '0';
				xi_t_1 := '0';
			else
				xi_t_1 := xi_t;
				xi_t := pulse;
			end if;
		end if;
		fa_pulse <= not xi_t_1 and xi_t;
		fd_pulse <= xi_t_1 and not xi_t;
	end process;
	
end Behavioral;

