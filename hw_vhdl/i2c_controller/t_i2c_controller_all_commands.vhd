LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY t_i2c_controller_all_commands IS
END t_i2c_controller_all_commands;
 
ARCHITECTURE behavior OF t_i2c_controller_all_commands IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT i2c_controller
    PORT(
         CLK_IN : IN  std_logic;
			CLK_I2C_AUX_3200K : in STD_LOGIC;
         RESET : IN  std_logic;
         SDA : INOUT  std_logic;
         SCL : INOUT  std_logic;
         SLAVE_ADDRESS : IN  std_logic_vector(6 downto 0);
         R_W : IN  std_logic;
         REG_ADDRESS : IN  std_logic_vector(7 downto 0);
         DATA_TO_WRITE : IN  std_logic_vector(7 downto 0);
         NEW_ORDER : IN  std_logic;
         CNTS_READ : IN  std_logic;
         I2C_BUSY : OUT  std_logic;
         ACK_ERROR : OUT  std_logic;
         HR_NEW_DATA : OUT  std_logic;
         HR_DATA : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK_IN : std_logic := '0';
   signal CLK_I2C_AUX_3200K : std_logic := '0';
   signal RESET : std_logic := '1';
   signal SLAVE_ADDRESS : std_logic_vector(6 downto 0) := (others => '0');
   signal R_W : std_logic := '0';
   signal REG_ADDRESS : std_logic_vector(7 downto 0) := (others => '0');
   signal DATA_TO_WRITE : std_logic_vector(7 downto 0) := (others => '0');
   signal NEW_ORDER : std_logic := '0';
   signal CNTS_READ : std_logic := '0';

	--BiDirs
   signal SDA : std_logic := 'Z';

 	--Outputs
   signal SCL : std_logic;
   signal I2C_BUSY : std_logic;
   signal ACK_ERROR : std_logic;
   signal HR_NEW_DATA : std_logic;
   signal HR_DATA : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant CLK_IN_period : time := 10 ns;
   constant CLK_I2C_AUX_3200K_period : time := 312.5 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: i2c_controller PORT MAP (
          CLK_IN => CLK_IN,
			 CLK_I2C_AUX_3200K => CLK_I2C_AUX_3200K,
          RESET => RESET,
          SDA => SDA,
          SCL => SCL,
          SLAVE_ADDRESS => SLAVE_ADDRESS,
          R_W => R_W,
          REG_ADDRESS => REG_ADDRESS,
          DATA_TO_WRITE => DATA_TO_WRITE,
          NEW_ORDER => NEW_ORDER,
          CNTS_READ => CNTS_READ,
          I2C_BUSY => I2C_BUSY,
          ACK_ERROR => ACK_ERROR,
          HR_NEW_DATA => HR_NEW_DATA,
          HR_DATA => HR_DATA
        );

   -- Clock process definitions
   CLK_I2C_AUX_3200K_process :process
   begin
		CLK_I2C_AUX_3200K <= '0';
		wait for CLK_I2C_AUX_3200K_period/2;
		CLK_I2C_AUX_3200K <= '1';
		wait for CLK_I2C_AUX_3200K_period/2;
   end process;
 
   CLK_IN_process :process
   begin
		CLK_IN <= '0';
		wait for CLK_IN_period/2;
		CLK_IN <= '1';
		wait for CLK_IN_period/2;
   end process;
	
   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		RESET <= '0';
		
      wait for CLK_IN_period*10 + 6 ns;
		SDA <= 'Z';
		
		wait for 50 us;
		
		      -- insert stimulus here 
		SLAVE_ADDRESS 	<= "1010111";
		R_W				<= '1';
		REG_ADDRESS		<= "11111111";
		CNTS_READ		<= '0';
		DATA_TO_WRITE	<= "10101010"; -- Da igual el valor, por si se lia la fsm
		
		NEW_ORDER		<= '1';
		
		wait for CLK_IN_period;
		
		SLAVE_ADDRESS 	<= "0000000";
		R_W				<= '0';
		REG_ADDRESS		<= "00000000";
		CNTS_READ		<= '0';
		DATA_TO_WRITE	<= "00000000"; 
		
		NEW_ORDER		<= '0';
		
		
		-- Generacion de ACKs
		wait for 22 us;
		
		SDA <= '0';
		wait for 2.3 us;
		
		SDA <= 'Z';
		wait for 20.1 us;
		
		SDA <= '0';
		wait for 2.3 us;
		
		SDA <= 'Z';
		wait for 22.6 us;
		
		SDA <= '0';
		wait for 2.3 us;
		
		SDA <= 'Z';				-- repeated start
		wait for 0.2 us;
		
		-- Metemos datos a SDA
		
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '1';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '1';
		
		wait for 2.5 us;
		SDA <= 'Z';
		
		-- y repetimos por si acaso
		
		wait for 23 us;
		      -- insert stimulus here 
		SLAVE_ADDRESS 	<= "1010111";
		R_W				<= '1';
		REG_ADDRESS		<= "11111111";
		CNTS_READ		<= '0';
		DATA_TO_WRITE	<= "10101010"; -- Da igual el valor, por si se lia la fsm
		
		NEW_ORDER		<= '1';
		
		wait for CLK_IN_period;
		
		SLAVE_ADDRESS 	<= "0000000";
		R_W				<= '0';
		REG_ADDRESS		<= "00000000";
		CNTS_READ		<= '0';
		DATA_TO_WRITE	<= "00000000"; 
		
		NEW_ORDER		<= '0';
		
		-- Generacion de ACKs
		wait for 22 us;
		
		SDA <= '0';
		wait for 2.3 us;
		
		SDA <= 'Z';
		wait for 20.1 us;
		
		SDA <= '0';
		wait for 2.3 us;
		
		SDA <= 'Z';
		wait for 22.6 us;
		
		SDA <= '0';
		wait for 2.3 us;
		
		SDA <= 'Z';				-- repeated start
		wait for 0.2 us;
		
		-- Metemos datos a SDA
		
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '1';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '1';
		
		wait for 2.5 us;
		SDA <= 'Z';
		
		-- y ahora con un error
		
		wait for 23 us;
		      -- insert stimulus here 
		SLAVE_ADDRESS 	<= "1010111";
		R_W				<= '1';
		REG_ADDRESS		<= "11111111";
		CNTS_READ		<= '0';
		DATA_TO_WRITE	<= "10101010"; -- Da igual el valor, por si se lia la fsm
		
		NEW_ORDER		<= '1';
		
		wait for CLK_IN_period;
		
		SLAVE_ADDRESS 	<= "0000000";
		R_W				<= '0';
		REG_ADDRESS		<= "00000000";
		CNTS_READ		<= '0';
		DATA_TO_WRITE	<= "00000000"; 
		
		NEW_ORDER		<= '0';
		
		-- Generacion de ACKs
		wait for 22 us;
		
		SDA <= '1';
		wait for 2.3 us;
		
		SDA <= 'Z';
		
		--ahora vamos a escribir
		
		wait for 20 us;
				      -- insert stimulus here 
		SLAVE_ADDRESS 	<= "1010111";
		R_W				<= '0';
		REG_ADDRESS		<= "10001110";
		CNTS_READ		<= '0';
		DATA_TO_WRITE	<= "10101010"; -- Da igual el valor, por si se lia la fsm
		
		NEW_ORDER		<= '1';
		
		wait for CLK_IN_period;
		
		SLAVE_ADDRESS 	<= "0000000";
		R_W				<= '0';
		REG_ADDRESS		<= "00000000";
		CNTS_READ		<= '0';
		DATA_TO_WRITE	<= "00000000"; 
		
		NEW_ORDER		<= '0';
		
		
		-- Generacion de ACKs
		wait for 22 us;
		
		SDA <= '0';
		wait for 2.3 us;
		
		SDA <= 'Z';
		wait for 20.1 us;
		
		SDA <= '0';
		wait for 2.3 us;
		
		SDA <= 'Z';
		wait for 20 us;
		
		SDA <= '0';
		wait for 2.3 us;
		
		SDA <= 'Z';				-- repeated start		
		
		--ahora vamos a leer 5 seguidos
		
		wait for 10 us;
				      -- insert stimulus here 
		SLAVE_ADDRESS 	<= "1010111";
		R_W				<= '1';
		REG_ADDRESS		<= "10001110";
		CNTS_READ		<= '1';
		DATA_TO_WRITE	<= "10101010"; -- Da igual el valor, por si se lia la fsm
		
		NEW_ORDER		<= '1';
		
		wait for CLK_IN_period;
		
		SLAVE_ADDRESS 	<= "0000000";
		R_W				<= '0';
		REG_ADDRESS		<= "00000000";
		CNTS_READ		<= '0';
		DATA_TO_WRITE	<= "00000000"; 
		
		NEW_ORDER		<= '0';
		
		
		-- Generacion de ACKs
		wait for 22 us;
		
		SDA <= 'Z';
		wait for 2.3 us;
		
		SDA <= 'Z';
		wait for 20.1 us;
		
		SDA <= '0';
		wait for 2.3 us;
		
		SDA <= 'Z';
		wait for 22.5 us;
		
		SDA <= '0';
		wait for 2.3 us;
		
		SDA <= 'Z';	
		
		wait for 0.2 us;
		
		-- Metemos datos a SDA
		
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '1';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '1';
		
		wait for 2.5 us;
		SDA <= 'Z';
		
				-- Metemos datos a SDA
		wait for 2.5 us;
		SDA <= '1';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '1';
		
		wait for 2.5 us;
		SDA <= '1';
		
		wait for 2.5 us;
		SDA <= '1';
		
		wait for 2.5 us;
		SDA <= '1';
		
		wait for 2.5 us;
		SDA <= '0';
		
		wait for 2.5 us;
		SDA <= '1';
		
		wait for 2.5 us;
		SDA <= 'Z';
      wait;
   end process;

END;
