LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY t_freq_divider IS
END t_freq_divider;
 
ARCHITECTURE behavior OF t_freq_divider IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT freq_divider
	 
    PORT(
         clock_in : IN  std_logic;
         reset : IN  std_logic;
         stop : IN  std_logic;
         clock_out : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clock_in : std_logic := '0';
   signal reset : std_logic := '1';
   signal stop : std_logic := '0';

 	--Outputs
   signal clock_out : std_logic;

   -- Clock period definitions
   constant clock_in_period : time := 312.5 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: freq_divider 		
		PORT MAP (
			clock_in => clock_in,
         reset => reset,
         stop => stop,
         clock_out => clock_out
        );

   -- Clock process definitions
   clock_in_process :process
   begin
		clock_in <= '0';
		wait for clock_in_period/2;
		clock_in <= '1';
		wait for clock_in_period/2;
   end process; 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      -- wait for 100 ns;	
		
		
      wait for clock_in_period*10 + clock_in_period/2;
		reset <= '0';
		
      -- insert stimulus here 
		wait for clock_in_period*20;
		
		stop <= '1';
		wait for clock_in_period*7;
		
		stop <= '0';		
				
      wait;
   end process;

END;
