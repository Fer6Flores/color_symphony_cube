int clockPin = 13;
int latchPin = 12;
int dataPin = 11;

int p1 = 2;
int p2 = 3;
int p3 = 4;
int p4 = 5;
int p5 = 6;

int i = 0;
int j = 0;

void setup() {
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);

  pinMode(p1, OUTPUT);
  pinMode(p2, OUTPUT);
  pinMode(p3, OUTPUT);
  pinMode(p4, OUTPUT);
  pinMode(p5, OUTPUT);

  digitalWrite(latchPin, LOW); //Puesta a 0 de los registros
  shiftOut(dataPin, clockPin, MSBFIRST, 0);
  shiftOut(dataPin, clockPin, MSBFIRST, 0);
  shiftOut(dataPin, clockPin, MSBFIRST, 0);
  shiftOut(dataPin, clockPin, MSBFIRST, 0);
  digitalWrite(latchPin, HIGH);
}

void loop() {

  for (j = 0; j < 1; j++) { //Cuenta atrás
    for (i = 0; i <= 200; i++) { //Cuenta atrás, número 5, primera
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 132);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 132);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 132);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 5, segunda
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 5, tercera
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 16);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 16);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 16);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 5, cuarta
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 4);
      shiftOut(dataPin, clockPin, MSBFIRST, 33);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 4);
      shiftOut(dataPin, clockPin, MSBFIRST, 33);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 4);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 4);
      shiftOut(dataPin, clockPin, MSBFIRST, 33);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 5, quinta
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 66);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 66);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 66);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }

    for (i = 0; i <= 200; i++) { //Cuenta atrás, número 4, primera
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 132);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 4, segunda
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 4, tercera
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 16);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 4, cuarta
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 4);
      shiftOut(dataPin, clockPin, MSBFIRST, 33);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 4);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 4);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 4, quinta
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 66);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }

    for (i = 0; i <= 200; i++) { //Cuenta atrás, número 3, primera
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 132);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 132);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 132);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 3, segunda
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 3, tercera
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 16);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 16);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 16);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 3, cuarta
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 4);
      shiftOut(dataPin, clockPin, MSBFIRST, 33);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 4);
      shiftOut(dataPin, clockPin, MSBFIRST, 33);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 4);
      shiftOut(dataPin, clockPin, MSBFIRST, 33);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 3, quinta
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 66);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 66);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 66);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }

    for (i = 0; i <= 200; i++) { //Cuenta atrás, número 2, primera
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 132);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 132);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 132);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 2, segunda
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 2, tercera
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 16);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 16);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 16);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 2, cuarta
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 4);
      shiftOut(dataPin, clockPin, MSBFIRST, 33);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 4);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 4);
      shiftOut(dataPin, clockPin, MSBFIRST, 33);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 4);
      shiftOut(dataPin, clockPin, MSBFIRST, 33);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 2, quinta
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 66);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 66);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 66);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }

    for (i = 0; i <= 200; i++) { //Cuenta atrás, número 1, primera
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 1, segunda
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 64);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 1, tercera
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 1, cuarta
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 100; i++) { //Cuenta atrás, número 1, quinta
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
  }

  for (j = 0; j <= 10; j++) { // Plano con giro horario
    for (i = 0; i <= 40; i++) { //Diagonal secundaria
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 240);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 15);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 3);
      shiftOut(dataPin, clockPin, MSBFIRST, 224);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 31);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 40; i++) { //Diagonal secundaria posterior
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 240);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 15);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 3);
      shiftOut(dataPin, clockPin, MSBFIRST, 224);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 31);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 40; i++) { //Horizontal
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 240);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 15);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 3);
      shiftOut(dataPin, clockPin, MSBFIRST, 224);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 31);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 40; i++) { //Diagonal principal anterior
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 240);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 15);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 3);
      shiftOut(dataPin, clockPin, MSBFIRST, 224);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 31);
      digitalWrite(p3, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 40; i++) { //Diagonal principal
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 240);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 15);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p5, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 3);
      shiftOut(dataPin, clockPin, MSBFIRST, 224);
      digitalWrite(p3, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 31);
      digitalWrite(p2, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 40; i++) { //Diagonal principal posterior
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 15);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p5, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 3);
      shiftOut(dataPin, clockPin, MSBFIRST, 224);
      digitalWrite(p2, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 40; i++) { //Vertical
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 40; i++) { //Diagonal secundaria anterior
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 15);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 124);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 3);
      shiftOut(dataPin, clockPin, MSBFIRST, 224);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
  }

  for (j = 0; j <= 10; j++) { //Marco con giro antihorario
    for (i = 0; i <= 40; i++) { //Marco diagonal secundario
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 240);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 31);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 40; i++) { //Marco diagonal secundario posterior
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 15);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 40; i++) { //Marco vertical
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 40; i++) { //Marco diagonal principal anterior
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 15);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p5, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 3);
      shiftOut(dataPin, clockPin, MSBFIRST, 224);
      digitalWrite(p2, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 40; i++) { //Marco diagonal principal
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 240);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p5, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p5, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 31);
      digitalWrite(p2, LOW);
      digitalWrite(p1, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 40; i++) { //Marco diagonal principal posterior
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 240);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p4, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 31);
      digitalWrite(p3, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 40; i++) { //Marco horizontal
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 240);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

       digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 31);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);
    }
    for (i = 0; i <= 40; i++) { //Marco diagonal secundario anterior
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 1);
      shiftOut(dataPin, clockPin, MSBFIRST, 240);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p1, LOW);
      digitalWrite(p2, LOW);
      digitalWrite(p3, LOW);
      digitalWrite(p4, LOW);
      digitalWrite(p5, LOW);
      digitalWrite(p2, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 8);
      shiftOut(dataPin, clockPin, MSBFIRST, 128);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p2, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

       digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 68);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 2);
      shiftOut(dataPin, clockPin, MSBFIRST, 32);
      digitalWrite(p3, LOW);
      digitalWrite(p3, HIGH);
      digitalWrite(latchPin, HIGH);

      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 0);
      shiftOut(dataPin, clockPin, MSBFIRST, 31);
      digitalWrite(p3, LOW);
      digitalWrite(p4, HIGH);
      digitalWrite(latchPin, HIGH);
    }
  }
    //Cubo marco expandiendose y moviendose
    //Serpiente moviendose 3D
}

