# color_symphony_cube

This project has the objective of creating a RGB LEDs cube based on a FPGA and indexable RGB LEDs with different possible extensions.

## Project members:
 
* Altium Godness: @aliciasilvap 

* Git maniac: @Fer6Flores

* Pro-teleco: @braissg

## Project folders organization
In the main folder there are the .gitignore and the README.md files. Futhermore, there are the next folders:
- doc: There is all the documentation of the project.
    - led_cube_5x5x5_initial_version: There is the doc of a previous led cube version done several years ago with an Arduino.
    - datasheets: There are all the datasheets and manuals needed for this project.
    - money_and_things: BOMs, budgets, reports and miscellaneous.
- hw_pcb: There is the Altium project with the schematic and the layout of the PCB.
- hw_vhdl: There is all the designed cores to be implemented in the Nexys 2.
- sw: There are all the scripts used in this projects for simulations, generation of animations and embedded software for ESP-32.

## Project steps (ticked if in progress):
- [X] Basic cube animations using a FPGA and a PCB designed.<br>
- [ ] Cube animations based on gyros and acccelerations detected by MPU-9250.<br>
- [ ] Bluetooth feature using ESP-32.<br>
- [ ] Add speaker to reproduce music received from Bluetooth.<br>
- [ ] Cube animations based on the FFTs of the music reproduced.<br>
